import axios from 'axios';

import { API_URL } from '../constants';

export const brands = async (variables: any) => {
    return await axios.post(API_URL, {
        query: `
            query {
                brands {
                    id
                    description
                }
            }
        `,
        variables,
    });
}

export const brand = async (variables: any) => {
    return await axios.post(API_URL, {
        query: `
            query($key: ID!) {
                brand(key: $key) {
                    description
                    slug
                }
            }
        `,
        variables,
    });
}

export const brandBySlug = async (variables: any) => {
    return await axios.post(API_URL, {
        query: `
            query($slug: String!) {
                brandBySlug(slug: $slug) {
                    description
                    slug
                }
            }
        `,
        variables,
    });
}
