import { brand, brands, brandBySlug } from './queries/brands';

describe("test add function", () => {
    it('should return all brands', async () => {
        const brandsResult: any = await brands({});
        // console.log(brands.data.data.brands);

        expect(brandsResult).toBeDefined();
        expect(brandsResult.data.data.brands.length).toBe(22);
    });

    it('should return a brand', async () => {
        const brandsResult: any = await brands({});
        const brandToSearch = brandsResult.data.data.brands[0];

        const brandResult: any = await brand({ key: brandToSearch.id });
        const brandToTest = brandResult.data.data.brand;

        expect(brandToTest).toBeDefined();
        expect(brandToSearch.description).toBe(brandToTest.description);
    });

    it('should return a brand with the slug "jimmychoo"', async () => {
        const slug = 'jimmychoo';
        const description = 'Jimmy Choo';

        const brandResult: any = await brandBySlug({ slug });
        const brandToTest = brandResult.data.data.brandBySlug;

        expect(brandToTest).toBeDefined();
        expect(brandToTest.description).toBe(description);
        expect(brandToTest.slug).toBe(slug);
    });

});
