"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CartItemResolvers = {
    Query: {
        cartItem: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getCartItem(key);
        },
        itemsUser: function (_, _a, _b) {
            var user = _a.user;
            var data = _b.data;
            return data.getItemsUser(user);
        }
    },
    Mutation: {
        addCartItem: function (_, _a, _b) {
            var quantity = _a.quantity, color = _a.color, size = _a.size, productKey = _a.productKey;
            var data = _b.data, me = _b.me;
            return data.addCartItem(quantity, color, size, productKey, me.id);
        },
        removeCartItem: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data, me = _b.me;
            return data.removeCartItem(key);
        }
    },
    CartItem: {
        product: function (cartItem, args, _a) {
            var data = _a.data;
            return data.getProduct(cartItem.product);
        },
        color: function (cartItem, args, _a) {
            var data = _a.data;
            return data.getColor(cartItem.color);
        },
    }
};
exports.default = CartItemResolvers;
