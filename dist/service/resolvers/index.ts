import categoryResolvers from './category';
import subcategoryResolvers from './subcategory';
import brandResolvers from './brand';
import productResolvers from './product';
import colorResolvers from './color';
import fileResolvers from './file';
import userResolvers from './user';
import cartItemResolvers from './cartItem';
import orderResolvers from './order';
import orderItemResolvers from './orderItem';

export default [
    categoryResolvers,
    subcategoryResolvers,
    brandResolvers,
    productResolvers,
    colorResolvers,
    fileResolvers,
    userResolvers,
    cartItemResolvers,
    orderResolvers,
    orderItemResolvers
];