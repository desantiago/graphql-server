"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SubCategoryResolvers = {
    Query: {
        subcategories: function (_, __, _a) {
            var data = _a.data;
            return data.getSubCategories();
        },
        subcategory: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getSubCategory(key);
        },
        subcategoryBySlug: function (_, _a, _b) {
            var slug = _a.slug;
            var data = _b.data;
            return data.getSubCategoryBySlug(slug);
        },
    },
    Mutation: {
        createSubCategory: function (_, _a, _b) {
            var description = _a.description, slug = _a.slug, categoryKey = _a.categoryKey;
            var data = _b.data;
            return data.addSubCategory(description, slug, categoryKey);
        }
    },
    SubCategory: {
        category: function (subcategory, args, _a) {
            var data = _a.data;
            return data.getCategory(subcategory.categoryKey);
        }
    }
};
exports.default = SubCategoryResolvers;
