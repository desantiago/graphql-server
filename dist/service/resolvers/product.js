"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProductResolvers = {
    Query: {
        products: function (_, _a, _b) {
            var category = _a.category, subCategory = _a.subCategory, brands = _a.brands, colors = _a.colors, sizes = _a.sizes, skip = _a.skip, first = _a.first;
            var data = _b.data;
            return data.getProducts(category, subCategory, brands, colors, sizes, skip, first);
        },
        aggregations: function (_, _a, _b) {
            var category = _a.category, subCategory = _a.subCategory, brands = _a.brands, colors = _a.colors, sizes = _a.sizes;
            var data = _b.data;
            return data.aggregations(category, subCategory, brands, colors, sizes);
        },
        search: function (_, _a, _b) {
            var term = _a.term;
            var data = _b.data;
            return data.searchProducts(term);
        },
        product: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getProduct(key);
        },
    },
    Mutation: {
        createProduct: function (_, _a, _b) {
            var input = _a.input;
            var data = _b.data;
            return data.addProduct(input);
        },
        updateProduct: function (_, _a, _b) {
            var key = _a.key, input = _a.input;
            var data = _b.data;
            return data.updateProduct(key, input);
        },
        deleteProduct: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.deleteProduct(key);
        }
    },
    Product: {
        brand: function (product, args, _a) {
            var data = _a.data;
            return data.getBrand(product.brand);
        },
        category: function (product, args, _a) {
            var data = _a.data;
            return data.getCategory(product.category);
        },
        subCategory: function (product, args, _a) {
            var data = _a.data;
            return data.getSubCategory(product.subCategory);
        },
        colors: function (product, args, _a) {
            var data = _a.data;
            return product.colors.map(function (color) { return data.getColor(color); });
        }
    },
    Images: {
        color: function (image, args, _a) {
            var data = _a.data;
            return data.getColor(image.color);
        },
        images: function (_a, args, _b) {
            var images = _a.images;
            var data = _b.data;
            return images.map(function (imageKey) { return data.getFile(imageKey); });
        }
    },
    Prices: {
        color: function (price, args, _a) {
            var data = _a.data;
            return data.getColor(price.color);
        }
    },
    Sizes: {
        color: function (size, args, _a) {
            var data = _a.data;
            return data.getColor(size.color);
        }
    }
};
exports.default = ProductResolvers;
