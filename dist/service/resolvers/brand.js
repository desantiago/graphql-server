"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BrandResolvers = {
    Query: {
        brands: function (_, __, _a) {
            var data = _a.data;
            return data.getBrands();
        },
        brand: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getBrand(key);
        },
        brandBySlug: function (_, _a, _b) {
            var slug = _a.slug;
            var data = _b.data;
            return data.getBrandBySlug(slug);
        },
    },
    Mutation: {
        createBrand: function (_, _a, _b) {
            var description = _a.description, slug = _a.slug;
            var data = _b.data;
            return data.addBrand(description, slug);
        }
    }
};
exports.default = BrandResolvers;
