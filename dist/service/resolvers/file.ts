import fs from 'fs';

const getFileName = (fileName: string): string => {
    let newFileName = fileName;
    let num = 1;
    while (fs.existsSync(`./uploadedFiles/${newFileName}`)) {
        let parts = newFileName.split('.');
        const extension = parts.pop();
        const name = newFileName.replace(`.${extension}`, '') + num;
        newFileName = `${name}.${extension}`;
        num += 1;
    }
    return newFileName;
}

const FileResolvers = {
    Query: {
        files: (_: any, __: any, { data }: any) => data.getFiles(),
        file: (_: any, { key }: any, { data }: any) => data.getFile(key),
    },
    Mutation: {
        createFile: (_: any, { filename, mimetype, encoding }: any, { data }: any) => {
            return data.addFile(filename, mimetype, encoding);
        },
        // singleUpload: (parent, args) => {
        singleUpload: async (_: any, args: any, { data }: any) => {
            console.log(args);
            const { createReadStream, filename, mimetype, encoding } = await args.file;
            const fileStream = createReadStream();
            fileStream.pipe(fs.createWriteStream(`./uploadedFiles/${filename}`));
            return args.file;
            // return file.then((file: any) => {
            //     const {createReadStream, filename, mimetype} = file
            //     const fileStream = createReadStream()

            //     fileStream.pipe(fs.createWriteStream(`./uploadedFiles/${filename}`))
            //     return file;
            // });
        },
        multiUpload: (_: any, args: any, { data }: any) => {
            console.log(args);

            return args.files.map(async (file: any) => {
                const { createReadStream, filename, mimetype, encoding } = await file;
                const fileStream = createReadStream();

                const newFileName = getFileName(filename);

                fileStream.pipe(fs.createWriteStream(`./uploadedFiles/${newFileName}`));
                console.log("saved ", newFileName);
                const r = data.addFile(newFileName, mimetype, encoding)
                console.log(r);
                return r;

                // return {
                //     filename: newFileName,
                //     mimetype,
                //     encoding
                // }
            });

            // return args.files;
        },

        // singleUploadStream: async (parent, args) => {
        // singleUploadStream: async (_: any, { file }: any, { data }: any) => {
        //     // const file = await args.file
        //     const {createReadStream, filename, mimetype} = file
        //     const fileStream = createReadStream()

        //     const uploadParams = {Bucket: 'apollo-file-upload-test', Key: filename, Body: fileStream};
        //     const result = await s3.upload(uploadParams).promise()

        //     console.log(result)
        //     return file;
        // }
    },
};

export default FileResolvers;