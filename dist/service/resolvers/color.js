"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ColorResolvers = {
    Query: {
        colors: function (_, __, _a) {
            var data = _a.data;
            return data.getColors();
        },
        color: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getColor(key);
        },
    },
    Mutation: {
        createColor: function (_, _a, _b) {
            var name = _a.name, hex = _a.hex;
            var data = _b.data;
            return data.addColor(name, hex);
        }
    }
};
exports.default = ColorResolvers;
