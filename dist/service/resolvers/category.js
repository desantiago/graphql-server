"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CategoryResolvers = {
    Query: {
        categories: function (_, __, _a) {
            var data = _a.data;
            return data.getCategories();
        },
        category: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getCategory(key);
        },
        categoryBySlug: function (_, _a, _b) {
            var slug = _a.slug;
            var data = _b.data;
            return data.getCategoryBySlug(slug);
        },
    },
    Mutation: {
        createCategory: function (_, _a, _b) {
            var description = _a.description, slug = _a.slug;
            var data = _b.data;
            return data.addCategory(description, slug);
        }
    },
    Category: {
        subcategories: function (category, args, _a) {
            var data = _a.data;
            return data.getSubCategoriesCategory(category.key);
        }
    }
};
exports.default = CategoryResolvers;
