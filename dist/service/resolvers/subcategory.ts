const SubCategoryResolvers = {
    Query: {
        subcategories: (_: any, __: any, { data }: any) => data.getSubCategories(),
        subcategory: (_: any, { key }: any, { data }: any) => data.getSubCategory(key),
        subcategoryBySlug: (_: any, { slug }: any, { data }: any) => data.getSubCategoryBySlug(slug),
    },
    Mutation: {
        createSubCategory: (_: any, { description, slug, categoryKey }: any, { data }: any) => {
            return data.addSubCategory(description, slug, categoryKey);
        }
    },
    SubCategory: {
        category: (subcategory: any, args: any, { data }: any) => {
            return data.getCategory(subcategory.categoryKey);
        }
    }
};

export default SubCategoryResolvers;
