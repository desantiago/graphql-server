
const BrandResolvers = {
    Query: {
        brands: (_: any, __: any, { data }: any) => data.getBrands(),
        brand: (_: any, { key }: any, { data }: any) => data.getBrand(key),
        brandBySlug: (_: any, { slug }: any, { data }: any) => data.getBrandBySlug(slug),
    },
    Mutation: {
        createBrand: (_: any, { description, slug }: any, { data }: any) => {
            return data.addBrand(description, slug);
        }
    }
};

export default BrandResolvers;