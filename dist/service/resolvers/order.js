"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrderResolvers = {
    Query: {
        orders: function (_, __, _a) {
            var data = _a.data;
            return data.getOrders();
        },
        order: function (_, _a, _b) {
            var key = _a.key;
            var data = _b.data;
            return data.getOrder(key);
        },
        ordersByUser: function (_, _a, _b) {
            var user = _a.user;
            var data = _b.data;
            return data.getOrdersByUser(user);
        },
    },
    Mutation: {
        createOrder: function (_, _a, _b) {
            var total = _a.total, user = _a.user, stripeId = _a.stripeId;
            var data = _b.data;
            return data.addOrder(total, user, stripeId);
        },
        checkOut: function (_, _a, _b) {
            var stripeId = _a.stripeId;
            var data = _b.data, me = _b.me;
            if (!me)
                return null;
            return data.checkOut(stripeId, me);
        }
    },
    Order: {
        orderItems: function (order, args, _a) {
            var data = _a.data;
            return data.getOrderItemByOrder(order.key);
        },
        user: function (order, args, _a) {
            var data = _a.data;
            return data.getUser(order.user);
        }
    }
};
exports.default = OrderResolvers;
