import { gql } from "apollo-server-express";

export default gql`
    type Order {
        key: ID!
        id: Int
        user: User
        total: Float
        stripeId: String
        date: String
        timeStamp: String
        orderItems: [OrderItem]
    }

    extend type Query {
        orders: [Order]
        ordersByUser(user: ID!): [Order]
        order(key: ID!): Order
    }

    extend type Mutation {
        createOrder(total: Float, user: String, stripeId: String): Order
        checkOut(stripeId: String): Order
    }
`;
