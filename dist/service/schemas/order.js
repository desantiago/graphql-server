"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    type Order {\n        key: ID!\n        id: Int\n        user: User\n        total: Float\n        stripeId: String\n        date: String\n        timeStamp: String\n        orderItems: [OrderItem]\n    }\n\n    extend type Query {\n        orders: [Order]\n        ordersByUser(user: ID!): [Order]\n        order(key: ID!): Order\n    }\n\n    extend type Mutation {\n        createOrder(total: Float, user: String, stripeId: String): Order\n        checkOut(stripeId: String): Order\n    }\n"], ["\n    type Order {\n        key: ID!\n        id: Int\n        user: User\n        total: Float\n        stripeId: String\n        date: String\n        timeStamp: String\n        orderItems: [OrderItem]\n    }\n\n    extend type Query {\n        orders: [Order]\n        ordersByUser(user: ID!): [Order]\n        order(key: ID!): Order\n    }\n\n    extend type Mutation {\n        createOrder(total: Float, user: String, stripeId: String): Order\n        checkOut(stripeId: String): Order\n    }\n"])));
var templateObject_1;
