"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    type CartItem {\n        key: ID!\n        quantity: Int\n        color: Color\n        size: String\n        price: Float\n        total: Float\n        user: User\n        product: Product\n    }\n\n    extend type Query {\n        cartItem(key: ID!): CartItem\n        itemsUser(user: ID!): [CartItem]\n    }\n\n    extend type Mutation {\n        addCartItem(quantity: Int!, color: ID!, size: String!, productKey: ID!): CartItem\n        removeCartItem(key: String!): CartItem\n    }\n"], ["\n    type CartItem {\n        key: ID!\n        quantity: Int\n        color: Color\n        size: String\n        price: Float\n        total: Float\n        user: User\n        product: Product\n    }\n\n    extend type Query {\n        cartItem(key: ID!): CartItem\n        itemsUser(user: ID!): [CartItem]\n    }\n\n    extend type Mutation {\n        addCartItem(quantity: Int!, color: ID!, size: String!, productKey: ID!): CartItem\n        removeCartItem(key: String!): CartItem\n    }\n"])));
var templateObject_1;
