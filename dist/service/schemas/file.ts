import { gql } from "apollo-server-express";

export default gql`
    scalar Upload

    type File {
        key: String!
        filename: String!
        mimetype: String!
        encoding: String!
    }

    extend type Query {
        files: [File]
        file(key: ID!): File
    }
    
    extend type Mutation {
        createFile(filename: String, mimetype: String, encoding: String): File
        singleUpload(file: Upload!): File!,
        multiUpload(files: Upload!): [File],
    }
`
