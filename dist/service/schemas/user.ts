import { gql } from "apollo-server-express";

export default gql`
    type User {
        id: ID!
        name: String!
        email: String!
        cartItems: [CartItem]
    }

    type Token {
        token: String!
    }

    extend type Query {
        user(id: ID!): User
        authenticatedUser: User
    }

    extend type Mutation {
        signUp(name: String!, email: String!, password: String!): Token!
        signIn(email: String!, password: String!): Token!
    }
`;
