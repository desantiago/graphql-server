"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    scalar Upload\n\n    type File {\n        key: String!\n        filename: String!\n        mimetype: String!\n        encoding: String!\n    }\n\n    extend type Query {\n        files: [File]\n        file(key: ID!): File\n    }\n    \n    extend type Mutation {\n        createFile(filename: String, mimetype: String, encoding: String): File\n        singleUpload(file: Upload!): File!,\n        multiUpload(files: Upload!): [File],\n    }\n"], ["\n    scalar Upload\n\n    type File {\n        key: String!\n        filename: String!\n        mimetype: String!\n        encoding: String!\n    }\n\n    extend type Query {\n        files: [File]\n        file(key: ID!): File\n    }\n    \n    extend type Mutation {\n        createFile(filename: String, mimetype: String, encoding: String): File\n        singleUpload(file: Upload!): File!,\n        multiUpload(files: Upload!): [File],\n    }\n"])));
var templateObject_1;
