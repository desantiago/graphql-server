import { gql } from "apollo-server-express";
import categorySchema from './category';
import subcategorySchema from './subcategory'
import brandSchema from './brand'
import productSchema from './product'
import colorSchema from './color'
import fileSchema from './file'
import userSchema from './user'
import cartItemSchema from './cartItem'
import orderSchema from './order'
import orderItemSchema from './orderItem'

const linkSchema = gql`
    type Query {
        _: Boolean
    }

    type Mutation {
        _: Boolean
    }
`;

export default [
    linkSchema,
    categorySchema,
    subcategorySchema,
    brandSchema,
    productSchema,
    colorSchema,
    fileSchema,
    userSchema,
    cartItemSchema,
    orderSchema,
    orderItemSchema
];