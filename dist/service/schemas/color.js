"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
var apollo_server_express_1 = require("apollo-server-express");
exports.default = apollo_server_express_1.gql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    type Color {\n        key: ID!\n        name: String\n        hex: String\n    }\n\n    extend type Query {\n        colors: [Color]\n        color(key: ID!): Color\n    }\n\n    extend type Mutation {\n        createColor(name: String, hex: String): Color\n    }\n"], ["\n    type Color {\n        key: ID!\n        name: String\n        hex: String\n    }\n\n    extend type Query {\n        colors: [Color]\n        color(key: ID!): Color\n    }\n\n    extend type Mutation {\n        createColor(name: String, hex: String): Color\n    }\n"])));
var templateObject_1;
