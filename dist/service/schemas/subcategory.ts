import { gql } from "apollo-server-express";

export default gql`
    type SubCategory {
        key: ID!
        description: String
        slug: String
        timeStamp: String
        categoryKey: String
        category: Category!
    }

    extend type Query {
        subcategories: [SubCategory]
        subcategory(key: ID!): SubCategory
        subcategoryBySlug(slug: String!): SubCategory
    }

    extend type Mutation {
        createSubCategory(description: String, slug: String, categoryKey: String): SubCategory
    }
`;
