import { gql } from "apollo-server-express";

export default gql`
    type Brand {
        key: ID!
        description: String
        slug: String
        timeStamp: String
    }

    extend type Query {
        brands: [Brand]
        brand(key: ID!): Brand
        brandBySlug(slug: String!): Brand
    }

    extend type Mutation {
        createBrand(description: String, slug: String): Brand
    }
`;
