"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var apollo_server_express_1 = require("apollo-server-express");
var http_1 = require("http");
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var cookies_1 = __importDefault(require("cookies"));
var compression_1 = __importDefault(require("compression"));
var cors_1 = __importDefault(require("cors"));
var schema_1 = require("./schema");
var Data_1 = __importDefault(require("./models/Data"));
var PORT = process.env.PORT || 3000;
var app = express_1.default();
app.use("*", cors_1.default());
var SECRET = 'secret1';
var getMe = function (req) {
    var token = req.headers['x-token'];
    if (!token)
        return null;
    try {
        return jsonwebtoken_1.default.verify(token, SECRET);
    }
    catch (e) {
        return null;
    }
};
app.use(compression_1.default());
var server = new apollo_server_express_1.ApolloServer({
    schema: schema_1.schema,
    context: function (_a) {
        var req = _a.req, res = _a.res;
        var cookies = new cookies_1.default(req, res);
        var me = getMe(req);
        return {
            cookies: cookies,
            data: Data_1.default.getInstance(),
            me: me,
            secret: SECRET
        };
    },
});
server.applyMiddleware({ app: app, path: "/graphql" });
var httpServer = http_1.createServer(app);
httpServer.listen({ port: PORT }, function () {
    return console.log("\uD83D\uDE80GraphQL-Server is running on http://localhost:3000/graphql");
});
