export interface User {
    id: string;
    name: string;
    email: string;
    password: string;
    timeStamp?: string;
}

export interface Token {
    token: string;
}

export interface File {
    key: string;
    filename: string;
    mimetype: string;
    encoding: string;
    timeStamp: string;
}

export interface Category {
    key: string;
    description: string;
    slug: string;
    timeStamp?: string;
}

export interface SubCategory {
    key: string;
    description: string;
    slug: string;
    timeStamp?: string;
    categoryKey: string;
}

export interface Brand {
    key: string;
    description: string;
    slug: string
    timeStamp?: string;
}

export interface Color {
    key: string;
    name: string;
    hex: string;
}

export interface Images {
    color: string;
    images: string[];
}

export interface Sizes {
    color: string;
    sizes: string[];
}

export interface Price {
    size: string;
    price: number;
}

export interface Prices {
    color: string;
    prices: Price[]
}

export interface Product {
    key: string;

    id: string;
    slug: string;

    name: string;
    description: string;
    sizeAndFit: string;

    price: number;
    startSize: number;
    endSize: number;

    details: string;
    colors: string[];

    images: Images[];
    sizes?: Sizes[];
    prices?: Prices[];

    // images: DynamicObject<string[]>;
    // sizes?: DynamicObject<number[]>;
    // price?: DynamicObject<DynamicObject<number>>;
    timeStamp?: string;
    brand: string;
    category: string;
    subCategory: string;
}

export interface AggregationCategory {
    category: Category | null
    subCategories: SubCategory[]
}

export interface Aggregations {
    count: number
    brands: Brand[]
    sizes: string[]
    colors: Color[]
    categories: AggregationCategory[]
}

export interface CartItem {
    key: string;
    quantity: number;
    color: string;
    size: string;
    price: number;
    total: number;
    user: string;
    product: string;
}

export interface Order {
    key: string;
    id: number;
    user: string;
    total: number;
    stripeId: string;
    date: string;
    timeStamp: string;
}

export interface OrderItemInput {
    quantity: number;
    color: string;
    size: string;
    price: number;
    total: number;
    user: string;
    product: string;
    order: string;
    cartItem: string;
}

export interface OrderItem {
    key: string;
    quantity: number;
    color: string;
    size: string;
    price: number;
    total: number;
    user: string;
    product: string;
    productName: string;
    order: string;
    cartItem: string;
    timeStamp: string;
}
