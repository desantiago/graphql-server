import {
    Category,
    SubCategory,
    Brand,
    Product,
    Aggregations,
    Color,
    File,
    User,
    Token,
    CartItem,
    Order,
    OrderItem,
    OrderItemInput,
    AggregationCategory,
} from './types';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import testData from './TestData'
import fs from 'fs';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { AuthenticationError, UserInputError } from 'apollo-server-errors';
import stripe from '../lib/stripe';

export default class LocalData {
    private static instance: LocalData;
    private categories: Category[];
    private subcategories: SubCategory[];
    private brands: Brand[];
    private products: Product[];
    private colors: Color[];
    private files: File[];
    private users: User[];
    private cartItems: CartItem[];
    private orders: Order[];
    private orderItems: OrderItem[];

    private constructor() {
        if (fs.existsSync(`./testfile.json`)) {
            const fileContents: string = fs.readFileSync(`./testfile.json`, { encoding: 'utf8', flag: 'r' });
            const data = JSON.parse(fileContents);
            this.categories = data.categories;
            this.subcategories = data.subcategories;
            this.brands = data.brands;
            this.products = data.products;
            this.colors = data.colors;
            this.files = data.files;
            this.users = data.users;
            this.cartItems = data.cartItems;
            this.orders = data.orders;
            this.orderItems = data.orderItems;

            console.log(this.categories.length);
            console.log(this.subcategories.length);
            console.log(this.brands.length);
            console.log(this.products.length);
            console.log(this.colors.length);
            console.log(this.files.length);
            console.log(this.cartItems.length);
        }
        else {
            const data = testData();
            this.categories = data.categories;
            this.subcategories = data.subcategories;
            this.brands = data.brands;
            this.products = data.products;
            this.colors = data.colors;
            this.files = data.images;
            this.users = data.users;
            this.cartItems = data.cartItems;
            this.orders = data.orders;
            this.orderItems = data.orderItems;
        }

        // const idCategory = uuidv4();
        // this.categories = [];
        // this.categories.push({
        //     key: idCategory,
        //     description: 'Shoes',
        //     timeStamp: '1'
        // });

        // const idSubCategoryPumps = uuidv4();
        // this.subcategories = [];
        // this.subcategories.push({
        //     key: idSubCategoryPumps,
        //     description: 'Pumps',
        //     timeStamp: '1',
        //     categoryKey: idCategory
        // });
        // const idSubCategoryFlats = uuidv4();
        // this.subcategories.push({
        //     key: idSubCategoryFlats,
        //     description: 'Flats',
        //     timeStamp: '1',
        //     categoryKey: idCategory
        // });

        // const idBrandJimmyChoo = uuidv4();
        // this.brands = [];
        // this.brands.push({
        //     key: idBrandJimmyChoo,
        //     description: 'Jimmy Choo',
        //     timeStamp: '1'
        // });

        // this.products = [];

        // // key: string;
        // // name: string;
        // // description: string;
        // // sizeAndFit: string;
        // // details: string;
        // // colors: Color[];
        // // sizes: DynamicObject<[number]>;
        // // images: DynamicObject<[string]>;
        // // price: DynamicObject<[number]>;

        // const product1: Product = {
        //     key: uuidv4(),
        //     name: 'Romy 85 Patent Leather Pumps',
        //     description: "Crafted in Italy, the label's pointed toe Romy pumps are timeless in black patent leather. This sleek pair works equally well with cocktail dresses and denim. 3.3\" stiletto heels. 100% leather. In black. Made in Italy.",
        //     sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        //     details: '',
        //     colors: [{
        //         name: 'black',
        //         hex: '#000'
        //     }],
        //     sizes: [{
        //         name: 'black',
        //         sizes: ['5','5.5','6','6.5','7','8','9','10','11','12']
        //     }],
        //     images: [{
        //         name: 'black', 
        //         images: ['image1.jpg','image2.jpg','image3.jpg','image4.jpg','image5.jpg']
        //     }],
        //     prices: [{
        //         name: 'black',
        //         prices: [
        //             { size: '5', price: 650 },
        //             { size: '5.5', price: 650 },
        //             { size: '6', price: 650 },
        //             { size: '6.5', price: 650 },
        //             { size: '7', price: 650 },
        //             { size: '8', price: 650 },
        //             { size: '9', price: 650 },
        //             { size: '10', price: 650 },
        //             { size: '11', price: 650 },
        //             { size: '12', price: 650 },
        //         ],
        //     }],
        //     brand: idBrandJimmyChoo,
        //     category: idCategory,
        //     subCategory: idSubCategoryPumps
        // }

        // this.products.push(product1);
    }

    public static getInstance(): LocalData {
        if (!LocalData.instance) {
            LocalData.instance = new LocalData();
        }
        return LocalData.instance;
    }

    private emptyCategory() {
        return {
            key: '',
            description: '',
            timeStamp: '',
            slug: '',
        }
    }

    public getCategories(): Category[] {
        return this.categories;
    }

    public getCategory(key: string): Category {
        return this.categories.find(category => category.key === key) || this.emptyCategory();
    }

    public getCategoryBySlug(slug: string): Category {
        return this.categories.find(category => category.slug === slug) || this.emptyCategory();
    }

    public addCategory(description: string, slug: string) {
        const category: Category = {
            key: uuidv4(),
            description,
            slug,
            timeStamp: moment().millisecond() + ''
        }

        this.categories.push(category);
        return category;
    }

    private emptySubCategory() {
        return {
            key: '',
            description: '',
            slug: '',
            timeStamp: '',
            categoryKey: ''
        }
    }

    public getSubCategories(): SubCategory[] {
        return this.subcategories;
    }

    public getSubCategory(key: string): SubCategory {
        return this.subcategories.find(subcategory => subcategory.key === key) || this.emptySubCategory();
    }

    public getSubCategoryBySlug(slug: string): SubCategory {
        return this.subcategories.find(subcategory => subcategory.slug === slug) || this.emptySubCategory();
    }

    public addSubCategory(description: string, slug: string, categoryKey: string): SubCategory {
        const subcategory: SubCategory = {
            key: uuidv4(),
            description,
            slug,
            timeStamp: moment().milliseconds() + '',
            categoryKey
        }

        this.subcategories.push(subcategory);
        return subcategory;
    }

    public getSubCategoriesCategory(categoryKey: string): SubCategory[] {
        return this.subcategories.filter(subcategory => subcategory.categoryKey === categoryKey) || this.emptySubCategory();
    }

    private emptyBrand() {
        return {
            key: '',
            description: '',
            slug: '',
            timeStamp: '',
        }
    }

    public getBrands(): Brand[] {
        return this.brands;
    }

    public getBrand(key: string): Brand {
        return this.brands.find(brand => brand.key === key) || this.emptyBrand();
    }

    public getBrandBySlug(slug: string): Brand {
        return this.brands.find(brand => brand.slug === slug) || this.emptyBrand();
    }

    public addBrand(description: string, slug: string): Brand {
        const brand: Brand = {
            key: uuidv4(),
            description,
            slug,
            timeStamp: moment().milliseconds() + '',
        }

        this.brands.push(brand);
        return brand;
    }

    private emptyColor(): Color {
        return {
            key: '',
            name: '',
            hex: '',
        }
    }

    public getColors(): Color[] {
        return this.colors;
    }

    public getColor(key: string): Color {
        return this.colors.find(color => color.key === key) || this.emptyColor();
    }

    public addColor(name: string, hex: string): Color {
        const color: Color = {
            key: uuidv4(),
            name,
            hex
        }

        this.colors.push(color);
        return color;
    }


    private emptyFile(): File {
        return {
            key: '',
            mimetype: '',
            encoding: '',
            timeStamp: moment().milliseconds() + '',
            filename: ''
        }
    }

    public getFiles(): File[] {
        return this.files;
    }

    public getFile(key: string): File {
        return this.files.find(file => file.key === key) || this.emptyFile();
    }

    public addFile(filename: string, mimetype: string, encoding: string): File {
        const file: File = {
            key: uuidv4(),
            filename,
            mimetype,
            encoding,
            timeStamp: moment().milliseconds() + '',
        }

        this.files.push(file);
        return file;
    }


    private emptyProduct() {
        return {
            key: uuidv4(),
            name: '',
            description: '',
            sizeAndFit: '',
            details: '',
            colors: [''],
            // sizes: {},
            images: [{ color: '', images: [] }],
            // price: {}
            timeStamp: '',
            brand: '',
            category: '',
            subCategory: '',

            id: '',
            slug: '',
            startSize: 0,
            endSize: 0,
            price: 0,
        }
    }

    public getProducts(category: string[], subCategory: string[], brands: string[], colors: string[], sizes: string[], skip: number, first: number): Product[] {
        console.log("parameters ", category, subCategory, brands, colors, sizes, skip, first);
        let result = this.products;
        //if (subCategory) {
        if (subCategory && Array.isArray(subCategory) && subCategory.length) {            
            // const subCategoryKey = this.getSubCategoryBySlug(subCategory).key;
            // result = result.filter(product => {
            //     return product.subCategory === subCategoryKey
            // });
            const subCategoryKeys = subCategory.map((subCat: string) => this.getSubCategoryBySlug(subCat).key);
            result = result.filter(product => {
                return subCategoryKeys.includes(product.subCategory)
            });
        }
        // else if (category) {
        else if (category && Array.isArray(category) && category.length) {
            const categoryKeys = category.map((cat: string) => this.getCategoryBySlug(cat).key);
            result = result.filter(product => {
                return categoryKeys.includes(product.category)
            });
            // const categoryData = this.getCategoryBySlug(category);
            // if (categoryData.key) {
            //     const categoryKey = this.getCategoryBySlug(category).key;
            //     result = result.filter(product => product.category === categoryKey);
            // }
            // else {
            //     const brandData = this.getBrandBySlug(category);
            //     // console.log(brandData);
            //     if (brandData.key) {
            //         const brandKey = brandData.key;
            //         result = result.filter(product => product.brand === brandKey);
            //     }
            // }
        }

        if (brands && Array.isArray(brands) && brands.length) {
            const brandKeys = brands.map(brand => this.getBrandBySlug(brand).key);
            result = result.filter(product => brandKeys.includes(product.brand));
            // result = result.filter(product => brands.includes(product.brand));
        }
        if (colors && Array.isArray(colors) && colors.length) {
            result = result.filter(product => {
                const intersection = product.colors.filter(color => colors.includes(color));
                return intersection.length;
            });
        }
        if (sizes && Array.isArray(sizes) && sizes.length) {
            result = result.filter(product => {
                const s = product.sizes?.filter(size => {
                    const intersection = sizes.filter(sizeTofind => size.sizes.includes(sizeTofind));
                    // console.log('intersection ', intersection);
                    return intersection.length;
                });
                // console.log('s', s);
                return s?.length;
            });
        }
        if (first !== undefined && skip !== undefined && skip >= 0 && first >= 1) {
            // console.log('skip---', skip, first);
            result = result.slice(skip, skip + first);
            // console.log(result.length);
        }
        console.log('length', result.length);
        return result;
    }

    public aggregations(category: string[], subCategory: string[], brands: string[], colors: string[], sizes: string[]): Aggregations {
        const results = this.getProducts(category, subCategory, brands, colors, sizes, -1, -1);

        const temp = results.reduce((aggs: any, product: Product) => {
            aggs.count += 1;

            const aggCat = aggs.categories.find((cat: any) => cat.category === product.category)
            if (!aggCat) {
                aggs.categories.push({
                    category: product.category,
                    subCategories: [product.subCategory]
                });
            }
            else {
                if (!aggCat.subCategories.includes(product.subCategory)) {
                    aggCat.subCategories.push(product.subCategory)
                }
            }
            

            if (!aggs.brands.includes(product.brand)) aggs.brands.push(product.brand);
            product.colors.forEach(color => {
                if (!aggs.colors.includes(color)) {
                    aggs.colors.push(color);
                }
            });
            product.sizes!.forEach(size => {
                size.sizes.forEach(sizeProduct => {
                    if (!aggs.sizes.includes(sizeProduct)) {
                        aggs.sizes.push(sizeProduct);
                    }
                });
            });

            return aggs;
        }, { count: 0, colors: [], brands: [], sizes: [], categories: [] });

        // console.log(temp.categories);

        let categories: AggregationCategory[] = [];
        temp.categories.forEach((aggCat: any) => {
            categories.push({
                category: this.categories.find((cat: Category) => cat.key === aggCat.category) || null,
                subCategories: aggCat.subCategories.map((aggSubCat: string) => this.subcategories.find(subCat => subCat.key === aggSubCat))
            });
        });

        const aggregations: Aggregations = {
            count: temp.count,
            colors: temp.colors.map((colorKey: string) => this.colors.find(color => color.key === colorKey)),
            brands: temp.brands.map((brandKey: string) => this.brands.find(brand => brand.key === brandKey)),
            categories,
            sizes: temp.sizes,
        }

        return aggregations;
    }

    public getProduct(key: string): Product {
        return this.products.find(product => product.key === key) || this.emptyProduct();
    }

    public addProduct(productInput: Product): Product {
        const categoryKey = this.subcategories.find(subcategory => subcategory.key === productInput.subCategory)?.categoryKey || '';

        const product: Product = {
            ...productInput,
            key: uuidv4(),
            category: categoryKey,
            timeStamp: moment().milliseconds() + ''
        }
        // console.log(product);
        this.products.push(product);

        this.saveTestFile();
        return product;
    }

    public updateProduct(key: string, productInput: Product): Product {
        const categoryKey = this.subcategories.find(subcategory => subcategory.key === productInput.subCategory)?.categoryKey || '';
        const product = this.getProduct(key);

        const updatedProduct = {
            ...product,
            ...productInput,
            key,
            category: categoryKey,
            timeStamp: moment().milliseconds() + ''
        }

        const index = this.products.findIndex(prod => prod.key === key);
        this.products[index] = updatedProduct;
        this.saveTestFile();

        return updatedProduct;
    }

    public deleteProduct(key: string): Product {
        const index = this.products.findIndex(prod => prod.key === key);
        const product: Product = this.products[index];
        this.products.splice(index, 1);
        this.saveTestFile();

        return product;
    }

    private saveTestFile() {
        const testData = {
            categories: this.categories,
            subcategories: this.subcategories,
            brands: this.brands,
            products: this.products,
            colors: this.colors,
            files: this.files
        }
        fs.writeFileSync('testfile.json', JSON.stringify(testData));
    }


    private emptyUser(): User {
        return {
            id: '',
            name: '',
            email: '',
            password: '',
            timeStamp: moment().milliseconds() + ''
        }
    }

    public getUser(id: string): User {
        return this.users.find(user => user.id === id) || this.emptyUser();
    }

    public getUserByUserName(name: string): User {
        return this.users.find(user => user.name === name) || this.emptyUser();
    }

    public getUserByEmail(email: string): User | undefined {
        return this.users.find(user => user.email === email);
    }

    private createToken(user: User, secret: string, expiresIn: string): string {
        // return '';
        const { id, email, name } = user;
        // console.log(jwt.sign({ id, email, username }, secret, { expiresIn }));
        return jwt.sign({ id, email, name }, secret, { expiresIn });
    }

    private generatePasswordHash(password: string): string {
        const saltRounds = 10;
        const hash = bcrypt.hashSync(password, saltRounds);
        // console.log(hash);
        return hash;
    }

    private validatePassword(loginPassword: string, password: string): boolean {
        return bcrypt.compareSync(loginPassword, password);
    }

    public signUp(name: string, email: string, password: string, secret: string): Token {
        const user: User = {
            id: uuidv4(),
            name,
            email,
            password: this.generatePasswordHash(password),
            timeStamp: moment().milliseconds() + ''
        };

        this.users.push(user);

        return {
            token: this.createToken(user, secret, '30m')
        }
    }

    public signIn(email: string, password: string, secret: string): Token {
        const user = this.getUserByEmail(email);
        console.log(user);

        if (!user) throw new UserInputError('Invalid user');
        const isValid = this.validatePassword(password, user.password);
        console.log(isValid);
        if (!isValid) throw new AuthenticationError('Invalid password');

        // console.log(cookies);
        let token = this.createToken(user, secret, '240m')
        // cookies.set('x-token', token, {
        //     httpOnly: true,
        //     // sameSite: "lax",
        //     // here we put 6 hours, but you can put whatever you want (the shorter the safer, but also more annoying)
        //     maxAge: 6 * 60 * 60,
        //     //secure: process.env.NODE_ENV === "production",
        // });

        // console.log('in cookies', cookies.get('x-token'));

        return {
            token
        }
    }


    // extend type Query {
    //     cartItem(key: ID!): CartItem
    //     itemsUser(user: ID!): [CartItem]
    // }

    // extend type Mutation {
    //     addCartItem(quantity: Int, productKey: ID): CartItem
    // }

    public getCartItem(key: string): CartItem | undefined {
        return this.cartItems.find(cartItem => cartItem.key === key);
    }

    public getItemsUser(userKey: string): CartItem[] {
        console.log("getItemsUser ", userKey)
        return this.cartItems.filter(cartItem => cartItem.user === userKey);
    }

    public addCartItem(quantity: number, color: string, size: string, productKey: string, userKey: string): CartItem {
        const product: Product = this.getProduct(productKey);

        const cartItem = this.cartItems.find(cartItem => cartItem.product === productKey);

        if (cartItem && cartItem.color === color && cartItem.size === size) {
            cartItem.quantity = cartItem.quantity + quantity;
            return cartItem;
        }
        else {
            const newCartItem: CartItem = {
                key: uuidv4(),
                quantity,
                color,
                size,
                price: product.price,
                total: quantity * product.price,
                user: userKey,
                product: productKey
            }

            this.cartItems.push(newCartItem);

            return newCartItem;
        }
    }

    public removeCartItem(key: string): CartItem | null {
        const index = this.cartItems.findIndex(cartItem => cartItem.key === key);
        if (index !== -1) {
            const cartItem = this.cartItems[index];
            this.cartItems.splice(index, 1);

            return cartItem;
        }
        return null
    }

    public searchProducts(term: string): Product[] {
        return this.products.filter(
            product => product.name.toLowerCase().includes(term) ||
                product.description.toLowerCase().includes(term) ||
                product.details.toLowerCase().includes(term))
    }


    public getOrders(): Order[] {
        return this.orders;
    }

    public getOrder(key: string): Order | undefined {
        return this.orders.find(order => order.key === key);
    }

    public getOrdersByUser(user: string): Order[] {
        return this.orders.filter(order => order.user === user);
    }

    private nextOrderId(): number {
        return this.orders.reduce((nextId, v) => {
            return (nextId > v.id ? nextId : v.id);
        }, 0);
    }

    public addOrder(total: number, user: string, stripeId: string): Order {
        const order: Order = {
            key: uuidv4(),
            id: this.nextOrderId() + 1,
            total,
            user,
            stripeId,
            date: moment().valueOf().toString(),
            timeStamp: moment().valueOf().toString(),
        }

        this.orders.push(order);

        return order;
    }

    public getOrderItem(key: string): OrderItem | undefined {
        return this.orderItems.find(orderItem => orderItem.key === key);
    }

    public getOrderItemByOrder(orderKey: string): OrderItem[] {
        return this.orderItems.filter(orderItem => orderItem.order === orderKey);
    }

    public addOrderItem(input: OrderItemInput): OrderItem {
        const product: Product = this.getProduct(input.product);
        const orderItem: OrderItem = {
            ...input,
            productName: product.name,
            key: uuidv4(),
            timeStamp: moment().valueOf().toString(),
        }

        this.orderItems.push(orderItem);

        return orderItem;
    }

    public async checkOut(stripeId: string, user: any): Promise<Order> {
        const cartItems: CartItem[] = this.getItemsUser(user.id);
        const total: number = cartItems.reduce((total, item: CartItem) => total + item.total, 0);

        const charge = await stripe.paymentIntents.create({
            amount: total,
            currency: 'usd',
            payment_method: stripeId,
            confirm: true,
            // receipt_email: 'jenny.rosen@example.com',
        }).catch(err => {
            console.log(err);
            throw new Error(err.message);
        });

        console.log(charge);

        const order: Order = this.addOrder(total, user.id, charge.id);

        cartItems.forEach((cartItem: CartItem) => {
            const input: OrderItemInput = {
                quantity: cartItem.quantity,
                size: cartItem.size,
                price: cartItem.price,
                color: cartItem.color,
                total: cartItem.total,
                product: cartItem.product,
                order: order.key,
                cartItem: cartItem.key,
                user: user.id
            };

            this.addOrderItem(input);
        });

        cartItems.forEach((cartItem: CartItem) => {
            this.removeCartItem(cartItem.key);
        });

        return order;
    }
}