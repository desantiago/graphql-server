import LocalData from './LocalData'
import {
    Category,
    SubCategory,
    Brand,
    Product,
    Aggregations,
    Color,
    File,
    User,
    Token,
    CartItem,
    Order,
    OrderItem,
    OrderItemInput
} from './types';

export default class Data {
    private static instance: Data;
    private d: LocalData;

    private constructor() {
        this.d = LocalData.getInstance();
    }

    public static getInstance(): Data {
        if (!Data.instance) {
            Data.instance = new Data();
        }

        return Data.instance;
    }

    public getCategories(): Category[] {
        return this.d.getCategories();
    }

    public getCategory(key: string): Category {
        return this.d.getCategory(key);
    }

    public getCategoryBySlug(slug: string): Category {
        return this.d.getCategoryBySlug(slug);
    }

    public addCategory(description: string, slug: string): Category {
        return this.d.addCategory(description, slug);
    }

    public getSubCategories(): SubCategory[] {
        console.log('getSubCategories');
        return this.d.getSubCategories();
    }

    public getSubCategory(key: string): SubCategory {
        return this.d.getSubCategory(key)
    }

    public getSubCategoryBySlug(slug: string): SubCategory {
        return this.d.getSubCategoryBySlug(slug);
    }

    public addSubCategory(description: string, slug: string, categoryKey: string): SubCategory {
        return this.d.addSubCategory(description, slug, categoryKey);
    }

    public getSubCategoriesCategory(categoryKey: string): SubCategory[] {
        return this.d.getSubCategoriesCategory(categoryKey)
    }

    public getBrands(): Brand[] {
        return this.d.getBrands();
    }

    public getBrand(key: string): Brand {
        return this.d.getBrand(key);
    }

    public getBrandBySlug(slug: string): Brand {
        return this.d.getBrandBySlug(slug);
    }

    public addBrand(description: string, slug: string): Brand {
        return this.d.addBrand(description, slug);
    }

    public getColors(): Color[] {
        return this.d.getColors();
    }

    public getColor(key: string): Color {
        return this.d.getColor(key);
    }

    public addColor(name: string, hex: string): Color {
        return this.d.addColor(name, hex);
    }

    public getFiles(): File[] {
        return this.d.getFiles();
    }

    public getFile(key: string): File {
        return this.d.getFile(key);
    }

    public addFile(filename: string, mimetype: string, encondig: string): File {
        return this.d.addFile(filename, mimetype, encondig);
    }



    public getProducts(category: string[], subcategory: string[], brands: string[], colors: string[], sizes: string[], skip: number, first: number): Product[] {
        return this.d.getProducts(category, subcategory, brands, colors, sizes, skip, first);
    }

    public aggregations(category: string[], subcategory: string[], brands: string[], colors: string[], sizes: string[]): Aggregations {
        return this.d.aggregations(category, subcategory, brands, colors, sizes);
    }

    public getProduct(key: string): Product {
        return this.d.getProduct(key);
    }

    public addProduct(productInput: Product): Product {
        return this.d.addProduct(productInput);
    }

    public updateProduct(key: string, productInput: Product): Product {
        return this.d.updateProduct(key, productInput);
    }

    public deleteProduct(key: string): Product {
        return this.d.deleteProduct(key);
    }

    public getUser(id: string): User {
        return this.d.getUser(id);
    }

    public signUp(name: string, email: string, password: string, secret: string): Token {
        return this.d.signUp(name, email, password, secret);
    }

    public signIn(email: string, password: string, secret: string, cookies: any): Token {
        return this.d.signIn(email, password, secret);
    }


    public getCartItem(key: string): CartItem | undefined {
        return this.d.getCartItem(key);
    }

    public getItemsUser(userKey: string): CartItem[] {
        return this.d.getItemsUser(userKey);
    }

    public addCartItem(quantity: number, color: string, size: string, productKey: string, userKey: string): CartItem {
        return this.d.addCartItem(quantity, color, size, productKey, userKey);
    }

    public removeCartItem(key: string): CartItem | null {
        return this.d.removeCartItem(key)
    }

    public searchProducts(key: string): Product[] {
        return this.d.searchProducts(key)
    }

    public getOrders(): Order[] {
        return this.d.getOrders();
    }

    public getOrder(key: string): Order | undefined {
        return this.d.getOrder(key);
    }

    public addOrder(total: number, user: string, stripeId: string) {
        return this.d.addOrder(total, user, stripeId);
    }

    public getOrderItem(key: string): OrderItem | undefined {
        return this.d.getOrderItem(key);
    }

    public getOrderItemByOrder(orderKey: string): OrderItem[] {
        return this.d.getOrderItemByOrder(orderKey);
    }

    public addOrderItem(input: OrderItemInput): OrderItem {
        return this.d.addOrderItem(input);
    }

    public checkOut(stripeId: string, user: any): Promise<Order> {
        return this.d.checkOut(stripeId, user);
    }
}