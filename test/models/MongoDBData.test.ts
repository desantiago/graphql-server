import moment from 'moment';
import jwt from 'jsonwebtoken';

import {
    connect,
    closeDatabase,
    clearDatabase
} from '../util/dbhandler';

import {
    IBrand,
    ICategory,
    IColor,
    IFile,
    IProduct,
    ISubCategory,
    IUser,
    ICartItem,
    IOrder,
    IOrderItem,
    File,
    OrderItem,
    Product,
    User,
    TemporalUser,
} from '../../src/models/ProductSchema';

import {
    insertCategory,
    insertSubCategory,
} from '../../src/mutations';

//import {
//    Category,
//    SubCategory,
//    Brand,
//File
//} from '../../src/models/types';
import * as types from '../../src/models/types';


import { saveDataForTests } from '../../src/testdb';
import MongoDBData from '../../src/models/MongoDBData';

import testData from '../../src/models/TestData';
import product from '../service/schemas/product';
import { applyExtensions } from '@graphql-tools/merge';

const data = MongoDBData.getInstance();
const test = testData();


//jest.mock('../../src/lib/stripeCharge'); // this happens automatically with automocking
//import stripeCharge from '../../src/lib/stripeCharge';

// jest.mock('../../src/lib/stripeCharge', () => {
//     return {
//         id: 'mockedId'
//     }
// });

// jest.mock("../../src/lib/stripeCharge", () => ({
//     stripeCharge: (stripeId: string, total: number) => {
//         return {
//             id: 'mockedId'
//         }
//     },
// }))

//jest.mock("../../src/lib/stripeCharge");
//const stripeChargeMock = stripeCharge as jest.MockedFunction<typeof stripeCharge>;

//import stripe from '../../src/lib/stripe';
///jest.mock('stripe');
//const mockedStripe = stripe as jest.Mocked<typeof stripe>;

//jest.mock('stripe');

// Stripe.mockImplementation(() => {
//     return {
//         paymentIntentss: () => {
//             throw new Error('Test error');
//         },
//     };
// });


/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
    await connect();
    await saveDataForTests();
    console.log('data saved ----');
});

/**
 * Clear all test data after every test.
 */
//  afterEach(async () => await clearDatabase());

/**
* Remove and close the db and server.
*/
afterAll(async () => await closeDatabase());

describe('categories', () => {
    it('can get all the categories correctly', async () => {
        const categories: ICategory[] = await data.getCategories();

        expect(categories.length).toBe(test.categories.length);
        categories.forEach((cat: ICategory, index: number) => {
            expect(cat.slug).toBe(test.categories[index].slug);
        });
    });

    it('can get a category by id', async () => {
        const categories: ICategory[] = await data.getCategories();
        const categoryToTest: ICategory = categories[0];
        const category: ICategory | null = await data.getCategory(categoryToTest.id);

        expect(category?.slug).toBe(categoryToTest.slug);
    });

    it('can get a category by slug', async () => {
        const categoryToTest: types.Category = test.categories[1];
        const category: ICategory | null = await data.getCategoryBySlug(categoryToTest.slug);

        expect(category?.slug).toBe(categoryToTest.slug);
        expect(category?.description).toBe(categoryToTest.description);
    });

    it('can get null when getting an inexistent category', async () => {
        const category: ICategory | null = await data.getCategoryBySlug('test');
        expect(category).toBeNull();
    });

});

describe('subCategories', () => {
    it('can get all the subCategories correctly', async () => {
        const subCategories: ISubCategory[] = await data.getSubCategories();

        expect(subCategories.length).toBe(test.subcategories.length);
        subCategories.forEach((subcat: ISubCategory, index: number) => {
            expect(subcat.slug).toBe(test.subcategories[index].slug);
        });
    });

    it('can get a subCategory by id', async () => {
        const subCategories: ISubCategory[] = await data.getSubCategories();
        const subCategoryToTest: ISubCategory = subCategories[0];
        const subCategory: ISubCategory | null = await data.getSubCategory(subCategoryToTest.id);

        expect(subCategory?.slug).toBe(subCategoryToTest.slug);
    });

    it('can get a subCategory by slug', async () => {
        const subCategoryToTest: types.SubCategory = test.subcategories[1];
        const subCategory: ISubCategory | null = await data.getSubCategoryBySlug(subCategoryToTest.slug);

        expect(subCategory?.slug).toBe(subCategoryToTest.slug);
        expect(subCategory?.description).toBe(subCategoryToTest.description);
    });

    it('can get null when getting an inexistent subCategory', async () => {
        const subCategory: ISubCategory | null = await data.getSubCategoryBySlug('test');
        expect(subCategory).toBeNull();
    });

    it('can get all the subCategories from a category', async () => {
        const categoryToTest: types.Category = test.categories[0];
        const subCategoriesToTest: types.SubCategory[] = test.subcategories.filter((subcat: any) => {
            return subcat.categoryKey === categoryToTest.key;
        });
        const category: ICategory | null = await data.getCategoryBySlug(categoryToTest.slug);
        const subCategories: ISubCategory[] = await data.getSubCategoriesCategory(category?.id);

        expect(subCategories.length).toBe(subCategoriesToTest.length);
        subCategories.forEach((subcat: ISubCategory, index: number) => {
            expect(subCategoriesToTest.find(subcatTest => subcatTest.slug === subcat.slug)).not.toBeNull();
        });
    });
});

/**
* Brands
*/
describe('brands ', () => {

    it('can get all the brands', async () => {
        const brands: IBrand[] = await data.getBrands();

        expect(brands.length).toBe(test.brands.length);
        brands.forEach((brand: IBrand, index: number) => {
            const brandToTest = test.brands[index];
            expect(brand.slug).toBe(brandToTest.slug);
        });
    });

    it('can get a brand by id', async () => {
        const brands: IBrand[] = await data.getBrands();
        const brandToTest: IBrand = brands[0];
        const brand: IBrand | null = await data.getBrand(brandToTest.id);

        expect(brand?.slug).toBe(brandToTest.slug);
    });

    it('can get a brand by slug', async () => {
        const brandToTest: types.Brand = test.brands[1];
        const brand: IBrand | null = await data.getBrandBySlug(brandToTest.slug);

        expect(brand?.slug).toBe(brandToTest.slug);
        expect(brand?.description).toBe(brandToTest.description);
    });

    it('can get null when getting an inexistent brand', async () => {
        const brand: IBrand | null = await data.getBrandBySlug('test');
        expect(brand).toBeNull();
    });

    /**
     * Tests that a valid color is created
     */
    it('can be created correctly', async () => {
        const insertedBrand: IBrand | null = await data.addBrand("Test Brand", "test-brand");
        expect(insertedBrand).toBeDefined();

        const brand: IBrand | null = await data.getBrand(insertedBrand?._id);
        expect(brand?.slug).toBe(insertedBrand?.slug);

        //const insertedRecord: IColor | null = await insertColor("Test Color", "#aaa");
        //expect(insertedRecord).toBeDefined();

        //const loadedRecord: IColor = await colorLoaders.colorById.load(insertedRecord?._id);
        //expect(loadedRecord.name).toEqual(insertedRecord?.name);
        //expect(async () => await insertColor("Test Color", "#aaa"))
        //    .not
        //    .toThrow();
    });

});

/**
* Colors
*/
describe('colors ', () => {

    it('can get all the colors', async () => {
        const colors: IColor[] = await data.getColors();

        expect(colors.length).toBe(test.colors.length);
        colors.forEach((color: IColor, index: number) => {
            const colorToTest = test.colors[index];
            expect(color.name).toBe(colorToTest.name);
        });
    });

    it('can get a color by id', async () => {
        const colors: IColor[] = await data.getColors();
        const colorToTest: IColor = colors[0];
        const color: IColor | null = await data.getColor(colorToTest.id);

        expect(color?.name).toBe(colorToTest.name);
    });

    //it('can get null when getting an inexistent color', async () => {
    //    const color: IColor | null = await data.getColor('');
    //    expect(color).toBeNull();
    //});

    /**
     * Tests that a valid color is created
     */
    it('can be created correctly', async () => {
        const insertedColor: IColor | null = await data.addColor("Test Color", "#aaa");
        expect(insertedColor).toBeDefined();

        const color: IColor | null = await data.getColor(insertedColor?._id);
        expect(color?.name).toBe(insertedColor?.name);
    });

});

/**
* Files
*/
describe('files ', () => {

    it('can get all the files', async () => {
        const files: IFile[] = await data.getFiles();

        expect(files.length).toBe(test.images.length);
        files.forEach((file: IFile, index: number) => {
            const fileToTest = test.images[index];
            expect(file.filename).toBe(fileToTest.filename);
        });
    });

    it('can get a file by id', async () => {
        const files: IFile[] = await data.getFiles();
        const fileToTest: IFile = files[0];
        const file: IFile | null = await data.getFile(fileToTest.id);

        expect(file?.filename).toBe(fileToTest.filename);
    });

    it('can add a file to a local server', async () => {
        const insertedFile: IFile | null = await data.addFile('test.jpg', 'jpeg', '7bit');
        expect(insertedFile).toBeDefined();

        const file: IFile | null = await data.getFile(insertedFile?.id);
        expect(file?.filename).toBe(insertedFile?.filename);
    });

    it('can add a file from cloudinary', async () => {
        const file: types.File = {
            mimetype: "image/jpeg",
            encoding: "7bit",
            timeStamp: "169",
            key: "d029cbff-cc17-4505-a16b-34835c84139f",
            filename: "ROMY85PAT_1.jpg",
            asset_id: "79fdb83d4f9c4d51b490ff0b2818bde0",
            public_id: "products/fap12hjyxpcpjpf3hyti",
            version: 1617845326,
            version_id: "9150bd69710077ab3e7b56b0c00184e5",
            signature: "7a2dfd7b715e98e78af22cbe96521cf47918518e",
            width: 1600,
            height: 2000,
            format: "jpg",
            resource_type: "image",
            created_at: "2021-04-08T01:28:46Z",
            bytes: 136605,
            etag: "c3978401e9f580c74b265866e23a1ec6",
            url: "http://res.cloudinary.com/dzsqpfo5u/image/upload/v1617845326/products/...",
            secure_url: "https://res.cloudinary.com/dzsqpfo5u/image/upload/v1617845326/products...",
            server: "cloudinary"
        }

        const insertedFile: IFile | null = await data.addFileFromCloudinary(file);
        expect(insertedFile).toBeDefined();

        const fileToTest: IFile | null = await data.getFile(insertedFile?.id);
        expect(fileToTest?.filename).toBe(insertedFile?.filename);
    });

    it('can delete a file correctly', async () => {
        const insertedFile: IFile | null = await data.addFile('test.jpg', 'jpeg', '7bit');
        expect(insertedFile).toBeDefined();

        const file: IFile | null = await data.getFile(insertedFile?.id);
        expect(file?.filename).toBe(insertedFile?.filename);

        await data.deleteFile(insertedFile?._id);

        const deletedFile: IFile | null = await File.findById(insertedFile?._id);
        //const deletedFile: IFile | null = await data.getFile(insertedFile?.id);
        expect(deletedFile).toBeNull();
    });

});

/**
* Products
*/

async function getProductTest(): Promise<types.Product> {
    const color: IColor | null = await data.addColor('Black', '#000');
    const idColorBlack = color?._id;

    const image = {
        filename: 'ROMY85PAT_1.jpg',
        mimetype: 'image/jpeg',
        encoding: '7bit',
        timeStamp: moment().milliseconds() + ''
    }
    const imgFile: IFile | null = await data.addFile(image.filename, image.mimetype, image.encoding);

    const brand: IBrand | null = await data.addBrand("Jimmy Choo", "jimmy-choo");
    const idBrandJimmyChoo = brand?._id;

    const category: ICategory | null = await insertCategory("Shoes", "shoes");
    const idCategoryShoes = category?._id;

    const subCategory: ISubCategory | null = await insertSubCategory(idCategoryShoes, "Pumps", "pumps");
    const idSubCategoryPumps = subCategory?._id;

    const product1: types.Product = {
        key: '',
        name: 'Romy 85 Patent Leather Pumps',
        description: "Crafted in Italy, the label's pointed toe Romy pumps are timeless in black patent leather. This sleek pair works equally well with cocktail dresses and denim. 3.3\" stiletto heels. 100% leather. In black. Made in Italy.",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: '',
        externalId: '',
        slug: 'jimmy-choo-romy-85-patent-leather-pumps',
        startSize: 5,
        endSize: 12,
        price: 65000,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: [imgFile?._id]
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 65000 },
                { size: '5.5', price: 65000 },
                { size: '6', price: 65000 },
                { size: '6.5', price: 65000 },
                { size: '7', price: 65000 },
                { size: '8', price: 65000 },
                { size: '9', price: 65000 },
                { size: '10', price: 65000 },
                { size: '11', price: 65000 },
                { size: '12', price: 65000 },
            ],
        }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps,
        mainImage: {
            color: idColorBlack,
            image: imgFile?._id,
        },
        thumbnail: {
            color: idColorBlack,
            image: imgFile?._id,
        }
    }

    return product1;
    // const insertedRecord: IProduct | null = await insertProduct(product1);
    // return insertedRecord;
}

describe('products', () => {
    it('can get products of the category shoes', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }

        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(32);
    });

    it('can get products of the category shoes and get the first 8 records', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const allProducts: IProduct[] = await data.getProductsByParams(params, 0, 0);

        const products: IProduct[] = await data.getProductsByParams(params, 0, 8);
        expect(products.length).toBe(8);

        for (let i = 0; i < products.length; i++) {
            const product1: IProduct = products[i];
            const product2: IProduct = allProducts[i];
            expect(product1.name).toBe(product2.name);
        }
    });

    it('can get products of the category shoes and get the second 8 records', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const allProducts: IProduct[] = await data.getProductsByParams(params, 0, 0);

        const products: IProduct[] = await data.getProductsByParams(params, 8, 8);
        expect(products.length).toBe(8);

        for (let i = 0; i < products.length; i++) {
            const product1: IProduct = products[i];
            const product2: IProduct = allProducts[i + 8];
            expect(product1.name).toBe(product2.name);
        }
    });

    it('can get products of the subcategory sandals', async () => {
        const params: types.QueryParams = {
            subCategory: ['sandals']
        }

        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(4);
    });

    it('can get products of the subcategory flats and boots', async () => {
        const params: types.QueryParams = {
            subCategory: ['flats', 'boots']
        }

        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(8);
    });

    it('can get products of the brand Jimmy Choo', async () => {
        const params: types.QueryParams = {
            brands: ['jimmychoo']
        }

        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(4);
    });

    it('can get products of color white', async () => {
        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');

        const params: types.QueryParams = {
            colors: [color?._id]
        }

        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(6);
    });

    it('can get products of the subcategory Pumps and brand Francesco Russo', async () => {
        const paramsPumps: types.QueryParams = {
            subCategory: ['pumps'],
        }
        const productsPumps: IProduct[] = await data.getProductsByParams(paramsPumps, 0, 0);
        expect(productsPumps.length).toBe(20);

        const paramsFrancesco: types.QueryParams = {
            brands: ['francescorusso']
        }
        const productsFrancesco: IProduct[] = await data.getProductsByParams(paramsFrancesco, 0, 0);
        expect(productsFrancesco.length).toBe(4);

        const params: types.QueryParams = {
            subCategory: ['pumps'],
            brands: ['francescorusso']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(2);
    });

    it('can get products of the subcategory Pumps and color White', async () => {
        const paramsPumps: types.QueryParams = {
            subCategory: ['pumps'],
        }
        const productsPumps: IProduct[] = await data.getProductsByParams(paramsPumps, 0, 0);
        expect(productsPumps.length).toBe(20);

        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');

        const paramsWhite: types.QueryParams = {
            colors: [color?._id]
        }
        const productsWhite: IProduct[] = await data.getProductsByParams(paramsWhite, 0, 0);
        expect(productsWhite.length).toBe(6);

        const params: types.QueryParams = {
            subCategory: ['pumps'],
            colors: [color?._id]
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(3);
    });

    it('can get products of the brand Francesco Russo and color White', async () => {
        const paramsFrancesco: types.QueryParams = {
            brands: ['francescorusso']
        }
        const productsFrancesco: IProduct[] = await data.getProductsByParams(paramsFrancesco, 0, 0);
        expect(productsFrancesco.length).toBe(4);

        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');
        const paramsWhite: types.QueryParams = {
            colors: [color?._id]
        }
        const productsWhite: IProduct[] = await data.getProductsByParams(paramsWhite, 0, 0);
        expect(productsWhite.length).toBe(6);

        const params: types.QueryParams = {
            brands: ['francescorusso'],
            colors: [color?._id]
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(1);
    });

    it('can get products of the SubCategory Pumps, brand Francesco Russo and color White', async () => {
        const paramsPumps: types.QueryParams = {
            subCategory: ['pumps'],
        }
        const productsPumps: IProduct[] = await data.getProductsByParams(paramsPumps, 0, 0);
        expect(productsPumps.length).toBe(20);

        const paramsFrancesco: types.QueryParams = {
            brands: ['francescorusso']
        }
        const productsFrancesco: IProduct[] = await data.getProductsByParams(paramsFrancesco, 0, 0);
        expect(productsFrancesco.length).toBe(4);

        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');
        const paramsWhite: types.QueryParams = {
            colors: [color?._id]
        }
        const productsWhite: IProduct[] = await data.getProductsByParams(paramsWhite, 0, 0);
        expect(productsWhite.length).toBe(6);

        const params: types.QueryParams = {
            subCategory: ['pumps'],
            brands: ['francescorusso'],
            colors: [color?._id]
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        expect(products.length).toBe(1);
    });

    it('can get the aggregations for the category: shoes ', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(32);
        expect(aggs.brands.length).toBe(13);
        expect(aggs.colors.length).toBe(10);
        expect(aggs.categories.length).toBe(1);
        expect(aggs.categories[0].subCategories.length).toBe(4);
        expect(aggs.mode).toBe('CATEGORY');
    });

    it('can get the aggregations for the category: sandals', async () => {
        const params: types.QueryParams = {
            subCategory: ['sandals']
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);
        ///console.log(aggs);

        expect(aggs.count).toBe(4);
        expect(aggs.brands.length).toBe(2);
        expect(aggs.colors.length).toBe(4);
        expect(aggs.mode).toBe('SUBCATEGORY');
    });

    it('can get the aggregations of the subcategory: flats and boots', async () => {
        const params: types.QueryParams = {
            subCategory: ['flats', 'boots']
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(8);
        expect(aggs.brands.length).toBe(6);
        expect(aggs.colors.length).toBe(4);
        expect(aggs.mode).toBe('SUBCATEGORY');
    });

    it('can get the aggregations of the brand Jimmy Choo', async () => {
        const params: types.QueryParams = {
            brands: ['jimmychoo']
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(4);
        expect(aggs.brands.length).toBe(1);
        expect(aggs.colors.length).toBe(4);
    });

    it('can get the aggregbations of color white', async () => {
        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');

        const params: types.QueryParams = {
            colors: [color?._id]
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(6);
        expect(aggs.brands.length).toBe(6);
        expect(aggs.colors.length).toBe(1);
    });

    it('can get products of the subcategory Pumps and brand Francesco Russo', async () => {
        const params: types.QueryParams = {
            subCategory: ['pumps'],
            brands: ['francescorusso']
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(2);
        expect(aggs.brands.length).toBe(1);
        expect(aggs.colors.length).toBe(2);
    });

    it('can get products of the subcategory Pumps and color White', async () => {
        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');

        const params: types.QueryParams = {
            subCategory: ['pumps'],
            colors: [color?._id]
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(3);
        expect(aggs.brands.length).toBe(3);
        expect(aggs.colors.length).toBe(1);
    });

    it('can get products of the brand Francesco Russo and color White', async () => {
        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');

        const params: types.QueryParams = {
            brands: ['francescorusso'],
            colors: [color?._id]
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(1);
        expect(aggs.brands.length).toBe(1);
        expect(aggs.colors.length).toBe(1);
    });

    it('can get products of the SubCategory Pumps, brand Francesco Russo and color White', async () => {
        const colors: IColor[] = await data.getColors();
        const color = colors.find(color => color.name === 'White');

        const params: types.QueryParams = {
            subCategory: ['pumps'],
            brands: ['francescorusso'],
            colors: [color?._id]
        }

        const aggs: types.Aggregations2 = await data.aggregationsByParams(params);

        expect(aggs.count).toBe(1);
        expect(aggs.brands.length).toBe(1);
        expect(aggs.colors.length).toBe(1);
    });

    it('can sort products ascending by name', async () => {
        const params: types.QueryParams = {
            subCategory: ['pumps'],
            sortBy: 'name',
            sortDirection: 'ASC'
        }

        const productsToTest: IProduct[] = await data.getProductsByParams(params, 0, 5);

        const subCategory = test.subcategories.find(subCat => subCat.slug === 'pumps');
        const productsToCompare = test.products
            .filter(product => product.subCategory === subCategory?.key)
            .sort((a, b) => a.name.localeCompare(b.name));

        expect(productsToTest.length).toBe(5);

        for (let i = 0; i < productsToTest.length; i++) {
            expect(productsToTest[i].name).toBe(productsToCompare[i].name);
        }
    });

    it('can sort products descending by name', async () => {
        const params: types.QueryParams = {
            subCategory: ['pumps'],
            sortBy: 'name',
            sortDirection: 'DESC'
        }

        const productsToTest: IProduct[] = await data.getProductsByParams(params, 0, 5);

        const subCategory = test.subcategories.find(subCat => subCat.slug === 'pumps');
        const productsToCompare = test.products
            .filter(product => product.subCategory === subCategory?.key)
            .sort((a, b) => -(a.name.localeCompare(b.name)));

        expect(productsToTest.length).toBe(5);

        for (let i = 0; i < productsToTest.length; i++) {
            expect(productsToTest[i].name).toBe(productsToCompare[i].name);
        }
    });


    it('can get a product by id', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }

        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const productToTest: IProduct = products[0];
        const product: IProduct | null = await data.getProduct(productToTest.id);

        expect(product?.name).toBe(productToTest.name);
    });

    it('can get a product by slug', async () => {
        const product: IProduct | null = await data.getProductByIds('jimmy-choo-romy-85-patent-leather-pumps');
        expect(product?.name).toBe('Romy 85 Patent Leather Pumps');
    });

    it('can insert a product', async () => {
        const productToInsert: types.Product = await getProductTest();
        const insertedProduct: IProduct | null = await data.addProduct(productToInsert);

        expect(insertedProduct).toBeDefined();

        const product: IProduct | null = await Product.findById(insertedProduct?._id);
        expect(insertedProduct?.name).toBe(product?.name);
    });

    it('can update a product', async () => {
        const productToUpdate: types.Product = await getProductTest();
        const insertedProduct: IProduct | null = await data.addProduct(productToUpdate);

        expect(insertedProduct).toBeDefined();

        productToUpdate.name = 'Updated Name';

        const updatedProduct: IProduct | null = await data.updateProduct(insertedProduct?._id, productToUpdate);

        const product: IProduct | null = await Product.findById(insertedProduct?._id);
        expect(updatedProduct?.name).toBe(product?.name);

        // product = await Product.findById(insertedProduct?._id);
        //expect(insertedProduct?.name).toBe(product?.name);
    });

    it('can delete a product', async () => {
        const productToInsert: types.Product = await getProductTest();
        const insertedProduct: IProduct | null = await data.addProduct(productToInsert);

        expect(insertedProduct).toBeDefined();

        const product: IProduct | null = await Product.findById(insertedProduct?._id);
        expect(insertedProduct?.name).toBe(product?.name);

        await data.deleteProduct(insertedProduct?._id);

        const deletedProduct: IProduct | null = await Product.findById(insertedProduct?._id);
        expect(deletedProduct).toBeNull();
    });

});

const SECRET = 'secret1';

interface TokenInterface {
    id: string;
    email: string;
    name: string;
}

const verifyToken = (token: string) => {
    try {
        return jwt.verify(token, SECRET);
    } catch (e) {
        return null;
    }
}

describe('users', () => {
    it('user can signup', async () => {
        const email = 'test@test.com';
        const token: types.Token | null = await data.signUp('Test Name', email, 'password', SECRET, '');
        expect(verifyToken(token ? token.token : '')).not.toBeNull();

        const user: IUser | null = await User.findOne({ 'email': email });
        expect(user).not.toBeNull();
        expect(user?.email).toBe(email);
    });

    it('temporal user can signup', async () => {
        const email = 'temp@temp.com';
        const token: types.Token | null = await data.signUpTemp(SECRET);
        expect(verifyToken(token ? token.token : '')).not.toBeNull();

        const user: IUser | null = await TemporalUser.findOne({ 'email': email });
        expect(user).not.toBeNull();
        expect(user?.email).toBe(email);

        TemporalUser.findByIdAndDelete(user?.id);
    });

    it('user can signin', async () => {
        const email = 'signintest@test.com';
        await data.signUp('Test Signin Name', email, 'password', SECRET, '');

        const token: types.Token | null = await data.signIn(email, 'password', SECRET, '');
        // expect(verifyToken(token ? token.token : '')).not.toBeNull();
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');
        expect(loggedUser).not.toBeNull();
        expect(loggedUser.email).toBe(email);
    });

    it('get an invalid token', async () => {
        const email = 'invalid@test.com';
        const token: types.Token | null = await data.signIn(email, 'password', SECRET, '');
        expect(verifyToken(token ? token.token : '')).toBeNull();
    });

    it('cart items added on temporal user are copied to the cart of a new user', async () => {
        // const tempEmail = 'temp@temp.com';
        const token: types.Token | null = await data.signUpTemp(SECRET);
        // expect(verifyToken(token ? token.token : '')).not.toBeNull();
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');
        expect(loggedUser).not.toBeNull();

        // const tempuser: IUser | null = await TemporalUser.findOne({ 'email': tempEmail });

        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser.id);

        const itemsTemp: ICartItem[] = await data.getItemsUser(loggedUser.id);
        expect(itemsTemp.length).toBe(1);

        const signUpEmail = 'signupcopycartitems@test.com';
        await data.signUp('Test Signup Copy', signUpEmail, 'password', SECRET, loggedUser.id);
        const user: IUser | null = await User.findOne({ 'email': signUpEmail });

        const items: ICartItem[] = await data.getItemsUser(user?.id);

        expect(items.length).toBe(1);
        expect(items[0].product).toBe(cartItem1?.product);

        TemporalUser.findByIdAndDelete(loggedUser.id);
    });

    it('cart items added on temporal user are copied to the cart when an existing user log in', async () => {
        // Create the user
        const signUpEmail = 'signincopycartitems@test.com';
        await data.signUp('Test Signin Copy', signUpEmail, 'password', SECRET, '');
        const user: IUser | null = await User.findOne({ 'email': signUpEmail });

        const itemsAfterSignUp: ICartItem[] = await data.getItemsUser(user?.id);
        expect(itemsAfterSignUp.length).toBe(0);

        // Create the temp user and add items to the 
        // const tempEmail = 'temp@temp.com';
        const token: types.Token | null = await data.signUpTemp(SECRET);
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');
        expect(loggedUser).not.toBeNull();

        //const tempuser: IUser | null = await TemporalUser.findOne({ 'email': tempEmail });

        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];
        const product2: IProduct = products[1];

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser.id);
        const cartItem2: ICartItem | null = await data.addCartItem(1, product2 && product2?.colors && product2?.colors.length ? product2?.colors[0] : '', '11', product2?.id, loggedUser.id);

        const itemsTemp: ICartItem[] = await data.getItemsUser(loggedUser.id);
        expect(itemsTemp.length).toBe(2);

        await data.signIn(signUpEmail, 'password', SECRET, loggedUser.id);
        // const user: IUser | null = await User.findOne({ 'email': signUpEmail });

        const items: ICartItem[] = await data.getItemsUser(user?.id);

        expect(items.length).toBe(2);
        expect(items[0].product).toBe(cartItem1?.product);
        expect(items[1].product).toBe(cartItem2?.product);

        TemporalUser.findByIdAndDelete(loggedUser.id);
    });

    it('if there are no temporal cart items, nothing is copied when the user logs in ', async () => {
        // const tempEmail = 'temp@temp.com';
        const token: types.Token | null = await data.signUpTemp(SECRET);
        // expect(verifyToken(token ? token.token : '')).not.toBeNull();
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');
        expect(loggedUser).not.toBeNull();

        // const tempuser: IUser | null = await TemporalUser.findOne({ 'email': tempEmail });
        const signUpEmail = 'signupnocopycartitems@test.com';
        await data.signUp('Test Signup Copy', signUpEmail, 'password', SECRET, loggedUser.id);
        const user: IUser | null = await User.findOne({ 'email': signUpEmail });

        const items: ICartItem[] = await data.getItemsUser(user?.id);
        expect(items.length).toBe(0);

        TemporalUser.findByIdAndDelete(loggedUser.id);
    });

    it('two temporal users add cartitems when on user logs in, only the products of the logged user products are copied ', async () => {
        const token1: types.Token | null = await data.signUpTemp(SECRET);
        // expect(verifyToken(token ? token.token : '')).not.toBeNull();
        const loggedUser1 = <TokenInterface>verifyToken(token1 ? token1.token : '');
        expect(loggedUser1).not.toBeNull();

        const token2: types.Token | null = await data.signUpTemp(SECRET);
        // expect(verifyToken(token ? token.token : '')).not.toBeNull();
        const loggedUser2 = <TokenInterface>verifyToken(token2 ? token2.token : '');
        expect(loggedUser2).not.toBeNull();

        // const tempuser: IUser | null = await TemporalUser.findOne({ 'email': tempEmail });

        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser1.id);

        const itemsUser1: ICartItem[] = await data.getItemsUser(loggedUser1.id);
        expect(itemsUser1.length).toBe(1);

        const product2: IProduct = products[1];
        const cartItem2: ICartItem | null = await data.addCartItem(1, product2 && product2?.colors && product2?.colors.length ? product2?.colors[0] : '', '11', product2?.id, loggedUser2.id);

        const itemsUser2: ICartItem[] = await data.getItemsUser(loggedUser2.id);
        expect(itemsUser2.length).toBe(1);


        const signUpEmail = 'signupcopycartitemsoneuser@test.com';
        await data.signUp('Test Signup Copy', signUpEmail, 'password', SECRET, loggedUser1.id);
        const user: IUser | null = await User.findOne({ 'email': signUpEmail });

        const items: ICartItem[] = await data.getItemsUser(user?.id);

        expect(items.length).toBe(1);
        expect(items[0].product).toBe(cartItem1?.product);

        TemporalUser.findByIdAndDelete(loggedUser1.id);
        TemporalUser.findByIdAndDelete(loggedUser2.id);
    });

});

describe('cart items', () => {
    it('items can be added to the cart', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];

        const email = 'additem@test.com';
        const token: types.Token | null = await data.signUp('Test Name', email, 'password', SECRET, '');
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');


        // const user: IUser | null = await User.findOne({ 'email': email });
        // expect(user).not.toBeNull();
        ///expect(user?.email).toBe(email);

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser.id);
        expect(product1?.id).toBe(cartItem1?.product);
        expect(cartItem1?.quantity).toBe(1);
    });

    it('the quantity can be updated when a same item is added', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];

        const email = 'updateitem@test.com';
        const token: types.Token | null = await data.signUp('Test Name', email, 'password', SECRET, '');
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');

        // const user: IUser | null = await User.findOne({ 'email': email });
        // expect(user).not.toBeNull();
        ///expect(user?.email).toBe(email);

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser.id);
        const cartItemUpdated: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser.id);

        const cartItem: ICartItem | null = await data.getCartItem(cartItemUpdated?.id);
        expect(cartItem?.quantity).toBe(2);

        const items: ICartItem[] = await data.getItemsUser(loggedUser.id);
        expect(items.length).toBe(1);
    });

    it('items with different sizes are added as independent cart items', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];

        const email = 'update2item@test.com';
        const token: types.Token | null = await data.signUp('Test Name', email, 'password', SECRET, '');
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');

        // const user: IUser | null = await User.findOne({ 'email': email });
        // expect(user).not.toBeNull();

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser.id);
        const loadedCartItem1: ICartItem | null = await data.getCartItem(cartItem1?.id);
        expect(loadedCartItem1?.quantity).toBe(1);

        const cartItem2: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '10', product1?.id, loggedUser.id);
        const loadedCartItem2: ICartItem | null = await data.getCartItem(cartItem2?.id);
        expect(loadedCartItem2?.quantity).toBe(1);

        const items: ICartItem[] = await data.getItemsUser(loggedUser.id);
        expect(items.length).toBe(2);
    });

    it('items can be removed from the cart', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];

        const email = 'additem@test.com';
        const token: types.Token | null = await data.signUp('Test Name', email, 'password', SECRET, '');
        const loggedUser = <TokenInterface>verifyToken(token ? token.token : '');

        // const user: IUser | null = await User.findOne({ 'email': email });
        // expect(user).not.toBeNull();
        ///expect(user?.email).toBe(email);

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, loggedUser.id);
        expect(product1?.id).toBe(cartItem1?.product);
        expect(cartItem1?.quantity).toBe(1);

        await data.removeCartItem(cartItem1?.id, loggedUser.id);

        const loadedCartItem1: ICartItem | null = await data.getCartItem(cartItem1?.id);
        expect(loadedCartItem1).toBeNull();
    });

});

const generateId = (): string => {
    const timestamp = (new Date().getTime() / 1000 | 0).toString(16);
    const oid = timestamp + 'xxxxxxxxxxxxxxxx'
        .replace(/[x]/g, _ => (Math.random() * 16 | 0).toString(16))
        .toLowerCase();
    return oid;
    //return { "$oid": oid };
}

describe('order and order items', () => {
    // it('can add an order and and order item', async () => {
    //     const params: types.QueryParams = {
    //         category: ['shoes']
    //     }
    //     const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
    //     const product1: IProduct = products[0];

    //     const email = 'addorderitem@test.com';
    //     const token: types.Token | null = await data.signUp('Test Name', email, 'password', SECRET);
    //     //expect(verifyToken(token ? token.token : '')).not.toBeNull();

    //     const user: IUser | null = await User.findOne({ 'email': email });
    //     expect(user).not.toBeNull();

    //     // const cartItemToInsert: types.CartItem = {
    //     //     key: '',
    //     //     quantity: 1,
    //     //     product: product1?._id,
    //     //     color: product1?.colors?.length ? product1.colors[0] : '',
    //     //     size: '11',
    //     //     price: 65000,
    //     //     total: 65000,
    //     //     user: user?._id
    //     // };

    //     const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, user?.id);

    //     // const cartItem: ICartItem | null = await insertCartItem(cartItemToInsert.quantity, cartItemToInsert.color, cartItemToInsert.size, cartItemToInsert.product, cartItemToInsert.price, cartItemToInsert.user);
    //     // expect(cartItem).toBeDefined();

    //     const order: IOrder | null = await data.addOrder(65000, user?._id, 'stripeId');
    //     expect(order).toBeDefined();

    //     const orderItem1: types.OrderItem = {
    //         key: '',
    //         quantity: 1,
    //         color: product1?.colors?.length ? product1.colors[0] : '',
    //         size: '11',
    //         price: 65000,
    //         total: 65000,
    //         user: user?._id,
    //         product: product1?._id,
    //         productName: product1?.name,
    //         order: order?._id,
    //         cartItem: cartItem1?._id,
    //         timeStamp: moment().valueOf().toString()
    //     }

    //     const insertedRecord: IOrderItem | null = await data.addOrderItem(orderItem1);
    //     expect(insertedRecord).toBeDefined();

    //     // const loadedRecord: IOrderItem = await orderItemLoaders.orderItemById.load(insertedRecord?._id);
    //     // expect(loadedRecord.productName).toEqual(insertedRecord?.productName);
    // });

    it('can get all orders, an order by id, an order item and all the items from an order', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];
        const product2: IProduct = products[1];
        const product3: IProduct = products[2];

        const email = 'getorderitem@test.com';
        await data.signUp('Test Name', email, 'password', SECRET, '');

        const user: IUser | null = await User.findOne({ 'email': email });
        expect(user).not.toBeNull();

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, user?.id);
        const cartItem2: ICartItem | null = await data.addCartItem(1, product2 && product2?.colors && product2?.colors.length ? product2?.colors[0] : '', '11', product2?.id, user?.id);
        const cartItem3: ICartItem | null = await data.addCartItem(1, product3 && product3?.colors && product3?.colors.length ? product3?.colors[0] : '', '11', product3?.id, user?.id);

        // const cartItem: ICartItem | null = await insertCartItem(cartItemToInsert.quantity, cartItemToInsert.color, cartItemToInsert.size, cartItemToInsert.product, cartItemToInsert.price, cartItemToInsert.user);
        // expect(cartItem).toBeDefined();
        const total1: number = product1.price && product2.price ? product1.price + product2.price : 0;
        const order1: IOrder | null = await data.addOrder(total1, user?._id, 'stripeId');
        expect(order1).toBeDefined();

        const total2: number = product3.price ? product3.price : 0;
        const order2: IOrder | null = await data.addOrder(total2, user?._id, 'stripeId');
        expect(order2).toBeDefined();

        const total3: number = product3.price ? product3.price : 0;
        const order3: IOrder | null = await data.addOrder(total3, user?._id, 'stripeId');
        expect(order3).toBeDefined();

        const orderItem1: types.OrderItem = {
            key: '',
            quantity: 1,
            color: product1?.colors?.length ? product1.colors[0] : '',
            size: '11',
            price: product1.price ? product1.price : 0,
            total: product1.price ? product1.price : 0,
            user: user?._id,
            product: product1?._id,
            productName: product1?.name,
            order: order1?._id,
            cartItem: cartItem1?._id,
            timeStamp: moment().valueOf().toString()
        }

        const insertedOrderItem1: IOrderItem | null = await data.addOrderItem(orderItem1);
        expect(insertedOrderItem1).toBeDefined();

        const orderItem2: types.OrderItem = {
            key: '',
            quantity: 1,
            color: product2?.colors?.length ? product2.colors[0] : '',
            size: '11',
            price: product2.price ? product2.price : 0,
            total: product2.price ? product2.price : 0,
            user: user?._id,
            product: product2?._id,
            productName: product2?.name,
            order: order1?._id,
            cartItem: cartItem2?._id,
            timeStamp: moment().valueOf().toString()
        }

        const insertedOrderItem2: IOrderItem | null = await data.addOrderItem(orderItem2);
        expect(insertedOrderItem2).toBeDefined();

        const orderItem3: types.OrderItem = {
            key: '',
            quantity: 1,
            color: product3?.colors?.length ? product3.colors[0] : '',
            size: '11',
            price: product3.price ? product3.price : 0,
            total: product3.price ? product3.price : 0,
            user: user?._id,
            product: product3?._id,
            productName: product3?.name,
            order: order2?._id,
            cartItem: cartItem3?._id,
            timeStamp: moment().valueOf().toString()
        }

        const insertedOrderItem3: IOrderItem | null = await data.addOrderItem(orderItem3);
        expect(insertedOrderItem3).toBeDefined();

        // const loadedRecord: IOrderItem = await orderItemLoaders.orderItemById.load(insertedRecord?._id);
        // expect(loadedRecord.productName).toEqual(insertedRecord?.productName);

        const orderList: IOrder[] = await data.getOrders();
        expect(orderList.length).toBe(3);
        expect(orderList[0].numAtCard).toBe(1)
        expect(orderList[1].numAtCard).toBe(2)
        expect(orderList[2].numAtCard).toBe(3)

        const orderLoaded1: IOrder | null = await data.getOrder(orderList[0].id);
        expect(orderLoaded1).toBeDefined();
        expect(orderLoaded1?.numAtCard).toBe(orderList[0].numAtCard);

        const itemsOrder1: IOrderItem[] = await data.getOrderItemByOrder(orderList[0].id);
        const itemsOrder2: IOrderItem[] = await data.getOrderItemByOrder(orderList[1].id);

        expect(itemsOrder1.length).toBe(2);
        expect(itemsOrder2.length).toBe(1);

        const orderItemLoaded1: IOrderItem | null = await data.getOrderItem(insertedOrderItem3?._id);
        expect(orderItemLoaded1).toBeDefined();
        expect(orderItemLoaded1?.product).toBe(insertedOrderItem3?.product);
    });

    it('can return null when getting an inexistent order', async () => {
        const orderLoaded: IOrder | null = await data.getOrder(generateId());
        expect(orderLoaded).toBeNull();
    });

    it('can return empty array when items from an inexistent order', async () => {
        const orderItemLoaded: IOrderItem[] = await data.getOrderItemByOrder(generateId());
        expect(orderItemLoaded.length).toBe(0);
    });

    it('can return null when getting an inexistent order item', async () => {
        const orderItemLoaded: IOrderItem | null = await data.getOrderItem(generateId());
        expect(orderItemLoaded).toBeNull();
    });
});

const equalsIgnoreOrder = (a: string[], b: string[]) => {
    if (a.length !== b.length) return false;
    const uniqueValues = Array.from(new Set([...a, ...b]));
    for (const v of uniqueValues) {
        const aCount = a.filter(e => e === v).length;
        const bCount = b.filter(e => e === v).length;
        if (aCount !== bCount) return false;
    }
    return true;
}

describe('checkout', () => {
    it('the user can checkout', async () => {
        const params: types.QueryParams = {
            category: ['shoes']
        }
        const products: IProduct[] = await data.getProductsByParams(params, 0, 0);
        const product1: IProduct = products[0];
        const product2: IProduct = products[1];
        const productIdList: string[] = [product1.id, product2.id];

        const email = 'checkout@test.com';
        await data.signUp('Test Name', email, 'password', SECRET, '');

        const user: IUser | null = await User.findOne({ 'email': email });
        expect(user).not.toBeNull();

        const cartItem1: ICartItem | null = await data.addCartItem(1, product1 && product1?.colors && product1?.colors.length ? product1?.colors[0] : '', '11', product1?.id, user?.id);
        const cartItem2: ICartItem | null = await data.addCartItem(1, product2 && product2?.colors && product2?.colors.length ? product2?.colors[0] : '', '11', product2?.id, user?.id);

        const order: IOrder | null = await data.checkOut('stripedId', user, true);
        expect(order).toBeDefined();
        const total1: number = product1.price && product2.price ? product1.price + product2.price : 0;
        expect(order?.total).toBe(total1);

        // console.log('finding orders ', order?.id);
        const itemsOrder: IOrderItem[] = await OrderItem.find({ order: order?.id });

        const productIdListOrder: string[] = itemsOrder.map((itemOrder: IOrderItem) => itemOrder.product);
        expect(itemsOrder.length).toBe(2);
        expect(equalsIgnoreOrder(productIdList, productIdListOrder)).toBeTruthy();

        const items: ICartItem[] = await data.getItemsUser(user?.id);
        expect(items.length).toBe(0);
    });
});