import moment from 'moment';

import {
    connect,
    closeDatabase,
    clearDatabase
} from '../util/dbhandler';

import {
    ICategory,
    IProduct,
    ISubCategory,
} from '../../src/models/ProductSchema';

import {
    deleteFile,
    insertBrand,
    insertCategory,
    insertColor,
    insertCartItem,
    insertFile,
    insertFileFromCloudinary,
    insertOrder,
    insertOrderItem,
    insertSubCategory,
    insertUser,
    insertTemporalUser,

    insertProduct,
    updateProduct,
    deleteProduct,
    deleteCartItem,
    updateCartItem
} from '../../src/mutations';

import colorLoaders from '../../src/dataloaders/colors';
import brandLoaders from '../../src/dataloaders/brands';
import categoryLoaders from '../../src/dataloaders/categories';
import cartItemLoaders from '../../src/dataloaders/cartItems';
import fileLoaders from '../../src/dataloaders/files';
import orderLoaders from '../../src/dataloaders/orders';
import orderItemLoaders from '../../src/dataloaders/orderItems';
import productLoaders from '../../src/dataloaders/products';
import subCategoryLoaders from '../../src/dataloaders/subCategories';
import userLoaders from '../../src/dataloaders/users';

import {
    IBrand,
    ICartItem,
    IColor,
    IFile,
    IOrder,
    IOrderItem,
    IUser,
} from '../../src/models/ProductSchema';

import * as types from '../../src/models/types';

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await connect());

/**
 * Clear all test data after every test.
 */
afterEach(async () => await clearDatabase());

/**
* Remove and close the db and server.
*/
afterAll(async () => await closeDatabase());

/**
 * Color.
 */
describe('color ', () => {

    /**
     * Tests that a valid color is created
     */
    it('can be created correctly', async () => {
        const insertedRecord: IColor | null = await insertColor("Test Color", "#aaa");
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IColor = await colorLoaders.colorById.load(insertedRecord?._id);
        expect(loadedRecord.name).toEqual(insertedRecord?.name);
    });

});

describe('brand', () => {
    it('can be created correctly', async () => {
        const insertedRecord: IBrand | null = await insertBrand("Test Brand", "test-brand");
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IBrand = await brandLoaders.brandById.load(insertedRecord?._id);
        expect(loadedRecord.slug).toEqual(insertedRecord?.slug);
    });
});

describe('file', () => {
    it('from local server can be created correctly', async () => {
        const insertedRecord: IFile | null = await insertFile('test.jpg', 'jpeg', '7bit');
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IFile = await fileLoaders.fileById.load(insertedRecord?._id);
        expect(loadedRecord.filename).toEqual(insertedRecord?.filename);
    });

    it('from cloudinary can be created correctly', async () => {
        const file: types.File = {
            mimetype: "image/jpeg",
            encoding: "7bit",
            timeStamp: "169",
            key: "d029cbff-cc17-4505-a16b-34835c84139f",
            filename: "ROMY85PAT_1.jpg",
            asset_id: "79fdb83d4f9c4d51b490ff0b2818bde0",
            public_id: "products/fap12hjyxpcpjpf3hyti",
            version: 1617845326,
            version_id: "9150bd69710077ab3e7b56b0c00184e5",
            signature: "7a2dfd7b715e98e78af22cbe96521cf47918518e",
            width: 1600,
            height: 2000,
            format: "jpg",
            resource_type: "image",
            created_at: "2021-04-08T01:28:46Z",
            bytes: 136605,
            etag: "c3978401e9f580c74b265866e23a1ec6",
            url: "http://res.cloudinary.com/dzsqpfo5u/image/upload/v1617845326/products/...",
            secure_url: "https://res.cloudinary.com/dzsqpfo5u/image/upload/v1617845326/products...",
            server: "cloudinary"
        }

        const insertedRecord: IFile | null = await insertFileFromCloudinary(file);
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IFile = await fileLoaders.fileById.load(insertedRecord?._id);
        expect(loadedRecord.filename).toEqual(insertedRecord?.filename);
    });

    it('delete file correctly', async () => {
        const insertedRecord: IFile | null = await insertFile('test.jpg', 'jpeg', '7bit');
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IFile = await fileLoaders.fileById.load(insertedRecord?._id);
        expect(loadedRecord.filename).toEqual(insertedRecord?.filename);

        await deleteFile(loadedRecord._id);
        fileLoaders.fileById.clearAll();

        const loadedRecordDeleted: IFile = await fileLoaders.fileById.load(insertedRecord?._id);
        expect(loadedRecordDeleted).toBeNull();
    });

});

async function insertProductTest(): Promise<IProduct | null> {
    const color: IColor | null = await insertColor('Black', '#000');
    const idColorBlack = color?._id;

    const image = {
        filename: 'ROMY85PAT_1.jpg',
        mimetype: 'image/jpeg',
        encoding: '7bit',
        timeStamp: moment().milliseconds() + ''
    }
    const imgFile: IFile | null = await insertFile(image.filename, image.mimetype, image.encoding);

    const brand: IBrand | null = await insertBrand("Jimmy Choo", "jimmy-choo");
    const idBrandJimmyChoo = brand?._id;

    const category: ICategory | null = await insertCategory("Shoes", "shoes");
    const idCategoryShoes = category?._id;

    const subCategory: ISubCategory | null = await insertSubCategory(idCategoryShoes, "Pumps", "pumps");
    const idSubCategoryPumps = subCategory?._id;

    const product1: types.Product = {
        key: '',
        name: 'Romy 85 Patent Leather Pumps',
        description: "Crafted in Italy, the label's pointed toe Romy pumps are timeless in black patent leather. This sleek pair works equally well with cocktail dresses and denim. 3.3\" stiletto heels. 100% leather. In black. Made in Italy.",
        sizeAndFit: "Item is listed in Italian sizing, please make note of accurate size conversions.",
        details: '',
        externalId: '',
        slug: 'jimmy-choo-romy-85-patent-leather-pumps',
        startSize: 5,
        endSize: 12,
        price: 65000,
        colors: [idColorBlack],
        sizes: [{
            color: idColorBlack,
            sizes: ['5', '5.5', '6', '6.5', '7', '8', '9', '10', '11', '12']
        }],
        images: [{
            color: idColorBlack,
            images: [imgFile?._id]
        }],
        prices: [{
            color: idColorBlack,
            prices: [
                { size: '5', price: 65000 },
                { size: '5.5', price: 65000 },
                { size: '6', price: 65000 },
                { size: '6.5', price: 65000 },
                { size: '7', price: 65000 },
                { size: '8', price: 65000 },
                { size: '9', price: 65000 },
                { size: '10', price: 65000 },
                { size: '11', price: 65000 },
                { size: '12', price: 65000 },
            ],
        }],
        brand: idBrandJimmyChoo,
        category: idCategoryShoes,
        subCategory: idSubCategoryPumps,
        mainImage: {
            color: idColorBlack,
            image: imgFile?._id,
        },
        thumbnail: {
            color: idColorBlack,
            image: imgFile?._id,
        }
    }

    const insertedRecord: IProduct | null = await insertProduct(product1);
    return insertedRecord;
}

describe('product ', () => {

    /**
     * Tests that a valid product is created
     */
    it('can be created correctly', async () => {
        const insertedRecord = await insertProductTest();
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IProduct | null = await productLoaders.productById.load(insertedRecord?._id);
        expect(loadedRecord?.name).toEqual(insertedRecord?.name);
    });

    it('can be updated correctly', async () => {
        const insertedRecord: IProduct | null = await insertProductTest();
        expect(insertedRecord).toBeDefined();

        const toUpdate: types.Product = {
            ...insertedRecord,
            name: 'changed name',
            key: '',
            //externalId: insertedRecord?.externalId,
            slug: insertedRecord?.slug,
            //description: insertedRecord?.description,
            description: 'test description',
            sizeAndFit: insertedRecord?.sizeAndFit,
            price: insertedRecord?.price,
            startSize: insertedRecord?.startSize,
            endSize: insertedRecord?.endSize,
            details: insertedRecord?.details,
            colors: insertedRecord?.colors,
            images: insertedRecord?.images,
            brand: insertedRecord?.brand,
            category: insertedRecord?.category,
            subCategory: insertedRecord?.subCategory,
            thumbnail: insertedRecord?.thumbnail,
            mainImage: insertedRecord?.mainImage
        }

        // const red = await updateProduct(insertedRecord?._id, toUpdate);

        expect(async () => await updateProduct(insertedRecord?._id, toUpdate))
            .not
            .toThrow();

        // const loadedRecord: IProduct | null = await productLoaders.productById.load(insertedRecord?._id);
        // expect(loadedRecord?.name).toEqual('changed name');
    });

    it('delete product correctly', async () => {
        const insertedRecord: IProduct | null = await insertProductTest();
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IProduct | null = await productLoaders.productById.load(insertedRecord?._id);
        expect(loadedRecord?.name).toEqual(insertedRecord?.name);

        await deleteProduct(loadedRecord?._id);
        productLoaders.productById.clearAll();

        const loadedRecordDeleted: IProduct | null = await productLoaders.productById.load(insertedRecord?._id);
        expect(loadedRecordDeleted).toBeNull();
    });
});

describe('category', () => {
    it('can be created correctly', async () => {
        const insertedRecord: ICategory | null = await insertCategory("Test Brand", "test-brand");
        expect(insertedRecord).toBeDefined();

        const loadedRecord: ICategory = await categoryLoaders.categoryById.load(insertedRecord?._id);
        expect(loadedRecord.slug).toEqual(insertedRecord?.slug);
    });
});

describe('subcategory', () => {
    it('can be created correctly', async () => {
        const category: ICategory | null = await insertCategory("Shoes", "shoes");
        const idCategoryShoes = category?._id;

        const insertedRecord: ISubCategory | null = await insertSubCategory(idCategoryShoes, "Test Brand", "test-brand");
        expect(insertedRecord).toBeDefined();

        const loadedRecord: ICategory = await subCategoryLoaders.subCategoryById.load(insertedRecord?._id);
        expect(loadedRecord.slug).toEqual(insertedRecord?.slug);
    });
});

describe('user', () => {
    it('user can be created correctly', async () => {
        const insertedRecord: IUser | null = await insertUser('name', 'name@mail.com', 'password');
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IUser = await userLoaders.userById.load(insertedRecord?._id);
        expect(loadedRecord.name).toEqual(insertedRecord?.name);
    });

    it('temporal user can be created correctly', async () => {
        const insertedRecord: IUser | null = await insertTemporalUser('temporal', 'temporal@temporal.com', 'password');
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IUser = await userLoaders.temporalUserById.load(insertedRecord?._id);
        expect(loadedRecord.name).toEqual(insertedRecord?.name);
    });
});

describe('order', () => {
    it('can be created correctly', async () => {
        const insertedRecord: IOrder | null = await insertOrder(1000, 'user', 'stripeId');
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IOrder = await orderLoaders.orderById.load(insertedRecord?._id);
        expect(loadedRecord.numAtCard).toEqual(1);

        const insertedRecord2: IOrder | null = await insertOrder(2000, 'user', 'stripeId');
        expect(insertedRecord2).toBeDefined();

        const loadedRecord2: IOrder = await orderLoaders.orderById.load(insertedRecord2?._id);
        expect(loadedRecord2.numAtCard).toEqual(2);
    });
});

async function insertCartItemTest(): Promise<ICartItem | null> {
    const product: IProduct | null = await insertProductTest();
    expect(product).toBeDefined();

    const user: IUser | null = await insertUser('name', 'name@mail.com', 'password');
    expect(user).toBeDefined();

    const cartItem: types.CartItem = {
        key: '',
        quantity: 1,
        product: product?._id,
        color: product?.colors?.length ? product.colors[0] : '',
        size: '11',
        price: 65000,
        total: 65000,
        user: user?._id
    };

    const insertedRecord: ICartItem | null = await insertCartItem(cartItem.quantity, cartItem.color, cartItem.size, cartItem.product, cartItem.price, cartItem.user);
    return insertedRecord;
}

describe('cart item', () => {
    it('can be created correctly', async () => {
        const insertedRecord: ICartItem | null = await insertCartItemTest();
        expect(insertedRecord).toBeDefined();

        const loadedRecord: ICartItem = await cartItemLoaders.cartItemById.load(insertedRecord?._id);
        expect(loadedRecord.product).toEqual(insertedRecord?.product);
    });

    it('can be deleted correctly', async () => {
        const insertedRecord: ICartItem | null = await insertCartItemTest();
        expect(insertedRecord).toBeDefined();

        const loadedRecord: ICartItem | null = await cartItemLoaders.cartItemById.load(insertedRecord?._id);
        expect(loadedRecord?.product).toEqual(insertedRecord?.product);

        await deleteCartItem(loadedRecord?._id);
        cartItemLoaders.cartItemById.clearAll();

        const loadedRecordDeleted: ICartItem | null = await cartItemLoaders.cartItemById.load(insertedRecord?._id);
        expect(loadedRecordDeleted).toBeNull();
    });

    it('can be updated correctly', async () => {
        const insertedRecord: ICartItem | null = await insertCartItemTest();
        expect(insertedRecord).toBeDefined();

        const loadedRecord: ICartItem | null = await cartItemLoaders.cartItemById.load(insertedRecord?._id);
        expect(loadedRecord?.product).toEqual(insertedRecord?.product);

        await updateCartItem(insertedRecord?._id, 2);
        cartItemLoaders.cartItemById.clearAll();

        const loadedRecordUpdated: ICartItem | null = await cartItemLoaders.cartItemById.load(insertedRecord?._id);
        expect(loadedRecordUpdated?.quantity).toEqual(2);
    });
});

describe('order item', () => {
    it('can be created correctly', async () => {

        const product: IProduct | null = await insertProductTest();
        expect(product).toBeDefined();

        const user: IUser | null = await insertUser('name', 'name@mail.com', 'password');
        expect(user).toBeDefined();

        const cartItemToInsert: types.CartItem = {
            key: '',
            quantity: 1,
            product: product?._id,
            color: product?.colors?.length ? product.colors[0] : '',
            size: '11',
            price: 65000,
            total: 65000,
            user: user?._id
        };

        const cartItem: ICartItem | null = await insertCartItem(cartItemToInsert.quantity, cartItemToInsert.color, cartItemToInsert.size, cartItemToInsert.product, cartItemToInsert.price, cartItemToInsert.user);
        expect(cartItem).toBeDefined();

        const order: IOrder | null = await insertOrder(1000, user?._id, 'stripeId');
        expect(order).toBeDefined();

        const orderItem1: types.OrderItem = {
            key: '',
            quantity: 1,
            color: product?.colors?.length ? product.colors[0] : '',
            size: '11',
            price: 65000,
            total: 65000,
            user: user?._id,
            product: product?._id,
            productName: 'Romy 85 Patent Leather Pumps',
            order: order?._id,
            cartItem: cartItem?._id,
            timeStamp: moment().valueOf().toString()
        }

        const insertedRecord: IOrderItem | null = await insertOrderItem(orderItem1);
        expect(insertedRecord).toBeDefined();

        const loadedRecord: IOrderItem = await orderItemLoaders.orderItemById.load(insertedRecord?._id);
        expect(loadedRecord.productName).toEqual(insertedRecord?.productName);
    });

});