const isTest = !!process.env.TEST_DATABASE;
const database = isTest ? process.env.TEST_DATABASE : 'myFirstDatabase';
console.log('DATABASE: ', database);
var mongoose = require('mongoose');
var mongoDB = `mongodb+srv://admin:${process.env.PASSWORD_DB}@cluster0.qlkxx.mongodb.net/${database}?retryWrites=true&w=majority`;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => console.log("connected to mongodb"));
