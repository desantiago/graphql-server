import bcrypt from 'bcrypt';
import moment from 'moment';

import {
    Brand,
    CartItem,
    Category,
    Color,
    File,
    Order,
    OrderItem,
    Product,
    SubCategory,
    TemporalUser,
    User,

    IBrand,
    ICartItem,
    IColor,
    IFile,
    IOrder,
    IOrderItem,
    IProduct,
    ISubCategory,
    IUser,
    ICategory,
} from '../models/ProductSchema';

import * as types from '../models/types';
import productLoaders from '../dataloaders/products';
import subCategoryLoaders from '../dataloaders/subCategories';

export async function updateCartItem(cartItemKey: string, quantity: number): Promise<ICartItem | null> {
    return await CartItem.findByIdAndUpdate(
        { _id: cartItemKey },
        { quantity }
    )
}

export async function insertCartItem(
    quantity: number,
    color: string,
    size: string,
    productKey: string,
    price: number,
    userKey: string): Promise<ICartItem | null> {

    const newCartItem: types.CartItem = {
        key: '',
        quantity,
        color,
        size,
        price: price,
        total: quantity * price,
        user: userKey,
        product: productKey
    }

    const model = new CartItem({
        ...newCartItem
    });

    return await model.save();
}

export async function deleteCartItem(cartItemKey: string): Promise<ICartItem | null> {
    return await CartItem.findByIdAndRemove(cartItemKey);
}

async function nextOrderId(): Promise<number> {
    const order: IOrder[] = await Order.find({}).sort({ numAtCard: -1 }).limit(1);
    if (!order[0]) return 1;
    // console.log("nextOrderId ", order[0]);
    return order[0].numAtCard + 1;
}

export async function insertOrder(total: number, user: string, stripeId: string): Promise<IOrder> {
    const model = new Order({
        numAtCard: await nextOrderId(),
        total,
        user,
        stripeId,
        date: moment().valueOf().toString(),
        timeStamp: moment().valueOf().toString(),
    });

    return model.save();
}

export async function insertOrderItem(input: types.OrderItemInput): Promise<IOrderItem | null> {
    const product: IProduct | null = await productLoaders.productById.load(input.product);
    if (!product) return null;

    const model = new OrderItem({
        ...input,
        productName: product.name,
        timeStamp: moment().valueOf().toString(),
    });

    return model.save();
}

export function generatePasswordHash(password: string): string {
    const saltRounds = 10;
    const hash = bcrypt.hashSync(password, saltRounds);
    return hash;
}

export async function insertUser(name: string, email: string, password: string): Promise<IUser | null> {
    const model = new User({
        name,
        email,
        password: generatePasswordHash(password),
        timeStamp: moment().valueOf().toString(),
    });

    return model.save();
}

export async function insertTemporalUser(name: string, email: string, password: string): Promise<IUser | null> {
    const model = new TemporalUser({
        name,
        email,
        password: password,
        timeStamp: moment().valueOf().toString(),
    });

    return model.save();
}


export async function insertProduct(productInput: types.Product): Promise<IProduct | null> {
    const subCategory: ISubCategory | null = await subCategoryLoaders.subCategoryById.load(productInput.subCategory);
    if (!subCategory) return null;

    const model = new Product({
        ...productInput,
        category: subCategory.categoryKey,
        timeStamp: moment().valueOf().toString(),
    });

    return model.save();
}

export async function updateProduct(id: string, productInput: types.Product): Promise<IProduct | null> {
    const subCategory: ISubCategory | null = await subCategoryLoaders.subCategoryById.load(productInput.subCategory);
    if (!subCategory) return null;

    return await Product.findByIdAndUpdate(
        id,
        {
            ...productInput,
            category: subCategory.categoryKey,
            timeStamp: moment().valueOf().toString(),
        },
        {
            new: true
        }
    );
}

export async function deleteProduct(id: string): Promise<IProduct | null> {
    return await Product.findByIdAndDelete(id);
}

export async function insertFile(filename: string, mimetype: string, encoding: string): Promise<IFile | null> {
    const model = new File({
        filename,
        mimetype,
        encoding,
        server: 'local',
        timeStamp: moment().valueOf().toString(),
    });
    return model.save();
}

export async function insertFileFromCloudinary(input: types.File): Promise<IFile | null> {
    const model = new File({
        ...input,
        server: 'cloudinary',
        timeStamp: moment().valueOf().toString(),
    });
    return model.save();
}

export async function deleteFile(id: string): Promise<IFile | null> {
    return await File.findByIdAndDelete(id);
}

export async function insertBrand(description: string, slug: string): Promise<IBrand | null> {
    const model = new Brand({
        description,
        slug,
        timeStamp: moment().valueOf().toString(),
    });
    return model.save();
}

export async function insertColor(name: string, hex: string): Promise<IColor | null> {
    const model = new Color({
        name,
        hex,
        timeStamp: moment().valueOf().toString(),
    });
    return model.save();
}

export async function insertCategory(description: string, slug: string): Promise<ICategory | null> {
    const model = new Category({
        description,
        slug,
        timeStamp: moment().valueOf().toString()
    })

    return model.save();
}

export async function insertSubCategory(categoryId: string, description: string, slug: string): Promise<ISubCategory | null> {
    const model = new SubCategory({
        description,
        slug,
        categoryKey: categoryId,
        timeStamp: moment().valueOf().toString()
    })

    return model.save();
}
