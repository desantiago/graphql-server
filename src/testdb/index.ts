import testData from '../models/TestData';

import {
    Product,
    Category,
    SubCategory,
    Brand,
    Color,
    File,
    User,
    Order,
    OrderItem,
    CartItem,

    ICategory,
} from '../models/ProductSchema';

// const isTest = !!process.env.TEST_DATABASE;

// if (isTest) {
//     console.log(`there is test ${process.env.TEST_DATABASE}`);
// }
// else {
//     console.log('NO TEST');
// }

interface Ids {
    [key: string]: string;
}

export async function saveData() {
    const data = testData();

    await Category.deleteMany({});
    await SubCategory.deleteMany({});
    await Brand.deleteMany({});
    await Color.deleteMany({});
    await User.deleteMany({});
    await File.deleteMany({});
    await Order.deleteMany({});
    await OrderItem.deleteMany({});
    await CartItem.deleteMany({});

    // console.log(data.categories);

    let categoryIds: Ids = await saveCategories(data);
    let subCategoryIds: Ids = await saveSubCategories(data, categoryIds);
    let brandIds: Ids = await saveBrands(data);
    let colorIds: Ids = await saveColors(data);
    let userIds: any = await saveUsers(data);
    let imageProdIds: any = await saveImages(data);

    let productIds: any = {};
    for (let i = 0; i < data.products.length; i++) {
        const product = data.products[i];
        const color = product.colors?.map(color => colorIds[color])[0];
        const productModel = new Product({
            ...product,
            colors: product.colors?.map(color => colorIds[color]),
            sizes: product.sizes?.map(size => {
                return {
                    ...size,
                    color: colorIds[size.color] || ''
                }
            }),
            images: product.images?.map(images => {
                return {
                    color: colorIds[images.color],
                    images: imageProdIds[i].map((id: string, index: number) => {
                        return {
                            file: id,
                            order: index
                        }
                    })
                }
            }),
            prices: product.prices?.map(prices => { return { ...prices, color: colorIds[prices.color] } }),
            brand: product.brand ? brandIds[product.brand] : '',
            category: product.category ? categoryIds[product.category] : '',
            subCategory: product.subCategory ? subCategoryIds[product?.subCategory] : '',
            thumbnail: {
                image: imageProdIds[i][0],
                color: color
            },
            mainImage: {
                image: imageProdIds[i][0],
                color: color
            }
        });
        const res = await productModel.save();
        productIds[product.key] = res._id;

    }

    let cartItemsId: any = {};
    for (let i = 0; i < data.cartItems.length; i++) {
        const cartItem = data.cartItems[i];
        const model = new CartItem({
            ...cartItem,
            product: productIds[cartItem.product],
            color: colorIds[cartItem.color],
            user: userIds[0]
        });
        const res = await model.save();
        cartItemsId[cartItem.key] = res._id;
    }

    for (let i = 0; i < data.orders.length; i++) {
        const order = data.orders[0];
        const key = order.key;

        const modelOrder = new Order({
            ...order,
            user: userIds[0]
        });

        const res = await modelOrder.save();
        const orderId = res._id;

        const orderItems = data.orderItems.filter(orderItem => orderItem.order === key);

        for (let j = 0; j < orderItems.length; j++) {
            const orderItem = orderItems[j];
            // console.log(orderItem.color);
            const modelOrderItems = new OrderItem({
                ...orderItem,
                color: colorIds[orderItem.color],
                user: userIds[0],
                product: productIds[orderItem.product],
                order: orderId,
                cartItem: cartItemsId[orderItem.cartItem],
            });

            await modelOrderItems.save();
        }
    }

    console.log("finished");
}

export async function saveDataForTests() {
    const data = testData();

    // await Category.deleteMany({});
    // await SubCategory.deleteMany({});
    // await Brand.deleteMany({});
    // await Color.deleteMany({});
    // await User.deleteMany({});
    // await File.deleteMany({});
    // await Order.deleteMany({});
    // await OrderItem.deleteMany({});
    // await CartItem.deleteMany({});

    // console.log(data.categories);

    let categoryIds: Ids = await saveCategories(data);
    let subCategoryIds: Ids = await saveSubCategories(data, categoryIds);
    let brandIds: Ids = await saveBrands(data);
    let colorIds: Ids = await saveColors(data);
    // let userIds: any = await saveUsers(data);
    let imageProdIds: any = await saveImages(data);

    let productIds: any = {};
    for (let i = 0; i < data.products.length; i++) {
        const product = data.products[i];
        const color = product.colors?.map(color => colorIds[color])[0];
        const productModel = new Product({
            ...product,
            colors: product.colors?.map(color => colorIds[color]),
            sizes: product.sizes?.map(size => {
                return {
                    ...size,
                    color: colorIds[size.color] || ''
                }
            }),
            images: product.images?.map(images => {
                return {
                    color: colorIds[images.color],
                    images: imageProdIds[i].map((id: string, index: number) => {
                        return {
                            file: id,
                            order: index
                        }
                    })
                }
            }),
            prices: product.prices?.map(prices => { return { ...prices, color: colorIds[prices.color] } }),
            brand: product.brand ? brandIds[product.brand] : '',
            category: product.category ? categoryIds[product.category] : '',
            subCategory: product.subCategory ? subCategoryIds[product?.subCategory] : '',
            thumbnail: {
                image: imageProdIds[i][0],
                color: color
            },
            mainImage: {
                image: imageProdIds[i][0],
                color: color
            }
        });
        const res = await productModel.save();
        productIds[product.key] = res._id;

    }

    console.log("finished");
}


export async function saveCategories(data: any): Promise<Ids> {
    let categoryIds: Ids = {};

    for (const category of data.categories) {
        const cat = new Category(category);
        const res = await cat.save();
        categoryIds[category.key] = res._id;
    }

    return categoryIds;
}

async function saveSubCategories(data: any, categoryIds: Ids): Promise<Ids> {
    let subCategoryIds: Ids = {};

    for (const subcategory of data.subcategories) {
        const category = data.categories.find((cat: any) => cat.key === subcategory.categoryKey);
        if (!category) continue;
        const subCat = new SubCategory({
            ...subcategory,
            categoryKey: categoryIds[category.key]
        });
        const res = await subCat.save();
        subCategoryIds[subcategory.key] = res._id;
    }

    return subCategoryIds;
}

async function saveBrands(data: any): Promise<Ids> {
    let brandsIds: Ids = {};

    for (const brand of data.brands) {
        const brandModel = new Brand(brand);
        const res = await brandModel.save();
        brandsIds[brand.key] = res._id;
    }

    return brandsIds;
}

async function saveColors(data: any): Promise<Ids> {
    let colorIds: Ids = {};

    for (const color of data.colors) {
        const colorModel = new Color(color);
        const res = await colorModel.save();
        colorIds[color.key] = res._id;
    }

    return colorIds;
}

async function saveUsers(data: any): Promise<any> {
    let userIds: any = [];

    for (const user of data.users) {
        const userModel = new User(user);
        const res = await userModel.save();
        userIds.push(res._id);
    }

    return userIds;
}

async function saveImages(data: any): Promise<any> {
    let imageProdIds: any = [];

    for (const images of data.imagesProduct) {
        let imageIds: String[] = [];
        for (const image of images) {
            // cloudinary.v2.uploader.upload(`../uploadedFile/${image.filename}`, 
            // const fileData: any = await uploadFile(`uploadedFiles/${image.filename}`);

            const fileModel = new File({
                ...image,
                // ...fileData,
                server: 'local'
            });
            const res = await fileModel.save();
            imageIds.push(res._id);
        }
        imageProdIds.push(imageIds);
    }

    return imageProdIds;
}