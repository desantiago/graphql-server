import { ServerContext } from "../../models/ProductSchema";
import { CartItem, Color, Order, OrderItem, Product, User } from "../../models/types";

const OrderItemResolvers = {
    Query: {
        orderItem: (_: void, { key }: OrderItem, { data }: ServerContext): Promise<OrderItem | null> => data.getOrderItem(key),
        orderItemByOrder: (_: void, { order }: OrderItem, { data }: ServerContext): Promise<OrderItem[]> => data.getOrderItemByOrder(order),
    },
    Mutation: {
        createOrderItem: (_: void, { input }: any, { data }: ServerContext): Promise<OrderItem | null> => {
            return data.addOrderItem(input);
        }
    },
    OrderItem: {
        color: (orderItem: OrderItem, args: void, { data }: ServerContext): Promise<Color | null> => {
            return data.getColor(orderItem.color)
        },
        user: (orderItem: OrderItem, _: void, { data }: ServerContext): Promise<User | null> => {
            return data.getUser(orderItem.user);
        },
        product: (orderItem: OrderItem, _: void, { data }: ServerContext): Promise<Product | null> => {
            return data.getProduct(orderItem.product);
        },
        order: (orderItem: OrderItem, _: void, { data }: ServerContext): Promise<Order | null> => {
            return data.getOrder(orderItem.order);
        },
        cartItem: (orderItem: OrderItem, _: any, { data }: ServerContext): Promise<CartItem | null> => {
            return data.getCartItem(orderItem.cartItem);
        }
    }
};

export default OrderItemResolvers;