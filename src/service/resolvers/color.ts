import { IColor, ServerContext } from "../../models/ProductSchema";
import { Color } from "../../models/types";

const ColorResolvers = {
    Query: {
        colors: (_: void, __: void, { data }: ServerContext): Promise<IColor[]> => data.getColors(),
        color: (_: void, { key }: Color, { data }: ServerContext): Promise<IColor | null> => data.getColor(key),
    },
    Mutation: {
        createColor: (_: void, { name, hex }: Color, { data }: ServerContext): Promise<IColor | null> => {
            return data.addColor(name, hex);
        }
    }
};

export default ColorResolvers;