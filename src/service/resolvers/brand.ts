import { IBrand, ServerContext } from "../../models/ProductSchema";
import { Brand } from "../../models/types";

const BrandResolvers = {
    Query: {
        brands: (_: void, __: void, { data }: ServerContext): Promise<IBrand[]> => data.getBrands(),
        brand: (_: void, { key }: any, { data }: ServerContext): Promise<IBrand | null> => data.getBrand(key),
        brandBySlug: async (_: void, { slug }: Brand, { data }: ServerContext): Promise<IBrand | null> => {
            return await data.getBrandBySlug(slug)
        },
    },
    Mutation: {
        createBrand: (_: void, { description, slug }: Brand, { data }: ServerContext): Promise<IBrand | null> => {
            return data.addBrand(description, slug);
        }
    }
};

export default BrandResolvers;