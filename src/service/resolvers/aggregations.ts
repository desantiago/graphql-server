import { IBrand, ICategory, IColor, ServerContext } from "../../models/ProductSchema";
import { Aggregations, Aggregations2, QueryParams, QueryParamsInput } from "../../models/types";

const AggregationsResolvers = {
    Query: {
        aggregations: (_: void, { category, subCategory, brands, colors, sizes }: QueryParams, { data }: ServerContext) => {
            return data.aggregations(
                category ? category : [],
                subCategory ? subCategory : [],
                brands ? brands : [],
                colors ? colors : [],
                sizes ? sizes : []);
        },
        aggregationsByParams: async (_: void, { where }: QueryParamsInput, { data }: ServerContext) => {
            return data.aggregationsByParams(where ? where : {});
        },
        aggregationsNoFilters: (_: void, { where }: QueryParamsInput, { data }: ServerContext) => {
            return data.aggregationsNoFilters(where ? where : {});
        },
    },
    Aggregations: {
        brands: (agg: Aggregations2, _: any, { data }: ServerContext): Promise<IBrand | null>[] => {
            return agg.brands.map((brand: string) => data.getBrand(brand))
        },
        colors: (agg: Aggregations2, _: any, { data }: ServerContext): Promise<IColor | null>[] => {
            return agg.colors.map((color: string) => data.getColor(color));
        },
        categories: (agg: Aggregations2, args: any, { data }: ServerContext) => {
            return agg.categories.map((cat: any) => {
                return {
                    category: data.getCategory(cat.category),
                    subCategories: cat.subCategories.map((subCat: string) => data.getSubCategory(subCat))
                };
            });
        },
    }
};

export default AggregationsResolvers;