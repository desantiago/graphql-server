import { ICategory, ISubCategory, ServerContext } from "../../models/ProductSchema";
import { Category } from "../../models/types";

const CategoryResolvers = {
    Query: {
        categories: (_: void, __: void, { data }: ServerContext): Promise<ICategory[]> => data.getCategories(),
        category: (_: void, { key }: Category, { data }: ServerContext): Promise<ICategory | null> => data.getCategory(key),
        categoryBySlug: (_: void, { slug }: Category, { data }: ServerContext): Promise<ICategory | null> => data.getCategoryBySlug(slug),
    },
    Mutation: {
        // createCategory: (_: void, { description, slug }: any, { data }: ServerContext) => {
        //     return data.addCategory(description, slug);
        // }
    },
    Category: {
        subcategories: (category: ICategory, _: void, { data }: ServerContext): Promise<ISubCategory[]> => {
            return data.getSubCategoriesCategory(category.id)
        }
    }
    // Book: {
    //     author: (book: any, args: any, { models }: any) => {
    //         return models.authors.find((author: any) => author.id === book.author) 
    //     }
    // },
};

export default CategoryResolvers;