import fs from 'fs';
import { IFile, ServerContext } from '../../models/ProductSchema';
import {
    File,
} from '../../models/types';

var cloudinary = require('cloudinary').v2;

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
});

const getFileName = (fileName: string): string => {
    let newFileName = fileName;
    let num = 1;
    while (fs.existsSync(`./uploadedFiles/${newFileName}`)) {
        let parts = newFileName.split('.');
        const extension = parts.pop();
        const name = newFileName.replace(`.${extension}`, '') + num;
        newFileName = `${name}.${extension}`;
        num += 1;
    }
    return newFileName;
}

let streamUpload = (fileStream: any): Promise<File> => {
    return new Promise((resolve, reject) => {
        let stream = cloudinary.uploader.upload_stream(
            {
                folder: 'products'
            },
            (error: any, result: any) => {
                if (result) {
                    resolve(result);
                } else {
                    reject(error);
                }
            }
        );

        fileStream.pipe(stream);
    });
}

const FileResolvers = {
    Query: {
        files: (_: void, __: void, { data }: ServerContext): Promise<IFile[]> => data.getFiles(),
        file: (_: void, { key }: File, { data }: ServerContext): Promise<IFile | null> => data.getFile(key),
    },
    Mutation: {
        createFile: (_: void, { filename, mimetype, encoding }: File, { data }: ServerContext): Promise<IFile | null> => {
            return data.addFile(filename, mimetype ? mimetype : '', encoding ? encoding : '');
        },
        deleteFile: async (_: void, { key }: File, { data }: ServerContext): Promise<IFile | null> => {
            const file: IFile | null = await data.getFile(key);
            console.log(file);
            await cloudinary.uploader.destroy(file?.public_id);
            return data.deleteFile(key);
        },
        // singleUpload: (parent, args) => {
        singleUpload: async (_: void, args: any, { data }: ServerContext) => {
            // console.log(args);
            const { createReadStream, filename, mimetype, encoding } = await args.file;
            const fileStream = createReadStream();
            fileStream.pipe(fs.createWriteStream(`./uploadedFiles/${filename}`));
            return args.file;
            // return file.then((file: any) => {
            //     const {createReadStream, filename, mimetype} = file
            //     const fileStream = createReadStream()

            //     fileStream.pipe(fs.createWriteStream(`./uploadedFiles/${filename}`))
            //     return file;
            // });
        },
        multiUpload: (_: void, args: any, { data }: ServerContext): Promise<IFile[]> => {
            console.log(args);

            return args.files.map(async (file: any) => {
                const { createReadStream, filename, mimetype, encoding } = await file;
                const fileStream = createReadStream();
                const newFileName = getFileName(filename);

                let result: File = await streamUpload(fileStream);
                fileStream.pipe(fs.createWriteStream(`./uploadedFiles/${newFileName}`));

                // const r = await data.addFile(newFileName, mimetype, encoding);
                const r = await data.addFileFromCloudinary({
                    ...result,
                    filename,
                    mimetype,
                    encoding
                });

                // console.log("file saved", r);
                return r;

                // return {
                //     filename: newFileName,
                //     mimetype,
                //     encoding
                // }
            });

            // return args.files;
        },

        // singleUploadStream: async (parent, args) => {
        // singleUploadStream: async (_: any, { file }: any, { data }: any) => {
        //     // const file = await args.file
        //     const {createReadStream, filename, mimetype} = file
        //     const fileStream = createReadStream()

        //     const uploadParams = {Bucket: 'apollo-file-upload-test', Key: filename, Body: fileStream};
        //     const result = await s3.upload(uploadParams).promise()

        //     console.log(result)
        //     return file;
        // }
    },
};

export default FileResolvers;