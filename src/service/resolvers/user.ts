import { IUser, ServerContext } from "../../models/ProductSchema";
import { CartItem, Token, User } from "../../models/types";

const UserResolvers = {
    Query: {
        user: (_: void, { id }: any, { data }: ServerContext): Promise<IUser | null> => {
            return data.getUser(id)
        },
        authenticatedUser: async (_: void, __: void, { data, me }: ServerContext): Promise<IUser | null> => {
            // console.log('authenticatedUser', me);
            // console.log('user', await data.getUser(me.id));
            // console.log('temp user', await data.getTemporalUser(me.id));
            if (me) return await data.getUser(me.id) || await data.getTemporalUser(me.id);
            return Promise.resolve(null);
        }
    },
    Mutation: {
        signUp: (_: void, { name, email, password }: User, { data, secret, me }: ServerContext): Promise<Token> => {
            return data.signUp(name, email, password, secret, me.id);
        },
        signIn: (_: void, { email, password }: User, { data, secret, me }: ServerContext): Promise<Token | null> => {
            return data.signIn(email, password, secret, me.id);
        },
        signUpTemp: (_: void, { }: User, { data, secret }: ServerContext): Promise<Token | null> => {
            return data.signUpTemp(secret);
        }
    },
    User: {
        cartItems: (user: IUser, args: void, { data }: ServerContext): Promise<CartItem[]> => {
            console.log('cartItems rel', user.id);
            return data.getItemsUser(user.id)
        }
    }
};

export default UserResolvers;
