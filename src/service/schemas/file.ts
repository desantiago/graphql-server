import { gql } from "apollo-server-express";

export default gql`
    scalar Upload

    type File {
        id: String
        key: String
        filename: String
        mimetype: String
        encoding: String

        asset_id: String
        public_id: String
        version: Int,
        version_id: String
        signature: String
        width: Int,
        height: Int,
        format: String
        resource_type: String
        created_at: String
        bytes: Int
        etag: String
        url: String
        secure_url: String

        server: String
    }

    extend type Query {
        files: [File]
        file(key: ID!): File
    }
    
    extend type Mutation {
        createFile(filename: String, mimetype: String, encoding: String): File
        deleteFile(key: String): File
        singleUpload(file: Upload!): File!,
        multiUpload(files: Upload!): [File],
    }
`
