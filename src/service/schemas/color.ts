import { gql } from "apollo-server-express";

export default gql`
    type Color {
        id: ID
        key: ID
        name: String
        hex: String
    }

    extend type Query {
        colors: [Color]
        color(key: ID!): Color
    }

    extend type Mutation {
        createColor(name: String, hex: String): Color
    }
`;
