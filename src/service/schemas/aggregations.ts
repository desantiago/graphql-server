import { gql } from "apollo-server-express";

export default gql`
    type AggregationCategory {
        category: Category
        subCategories: [SubCategory]
    }

    type Aggregations {
        count: Int
        brands: [Brand]
        sizes: [String]
        colors: [Color]
        categories: [AggregationCategory]
        mode: String
    }

    extend type Query {
        aggregations(category: [String], subCategory: [String], brands: [String], colors: [String], sizes: [String]): Aggregations
        aggregationsByParams(where: WhereInput): Aggregations
        aggregationsNoFilters(where: WhereInput): Aggregations
    }
`