import { gql } from "apollo-server-express";

export default gql`
    type ImageColor {
        color: Color
        image: File
    }

    type Image {
        file: File
        order: Int
    }

    type Images {
        color: Color
        images: [Image]
    }

    type Sizes {
        color: Color
        sizes: [String]
    }

    type Price {
        size: String
        price: Float
    }

    type Prices {
        color: Color
        prices: [Price]
    }

    type RelatedProduct {
        product: Product
        order: Int
    }

    type Product {
        id: String
        key: ID!
        name: String
        description: String
        sizeAndFit: String
        details: String
        timeStamp: String
        externalId: String
        slug: String
        startSize: Int,
        endSize: Int,
        price: Float,

        colors: [Color]
        images: [Images]
        sizes: [Sizes]
        prices: [Prices]

        category: Category
        subCategory: SubCategory
        brand: Brand

        thumbnail: ImageColor
        mainImage: ImageColor
        relatedProducts: [RelatedProduct]
    }


    input ColorInput {
        name: String
        hex: String
    }

    input ImageColorInput {
        color: String
        image: String
    }

    input ImageInput {
        file: String
        order: Int
    }

    input ImagesInput {
        color: String
        images: [ImageInput]
    }

    input SizesInput {
        color: String
        sizes: [String]
    }

    input PriceInput {
        size: String
        price: Float
    }

    input PricesInput {
        color: String
        prices: [PriceInput]
    }

    input RelatedProductInput {
        product: String
        order: Int
    }

    input ProductInput {
        key: ID
        name: String!
        description: String!
        sizeAndFit: String
        details: String
        timeStamp: String
        externalId: String
        slug: String
        startSize: Int,
        endSize: Int,
        price: Float,

        colors: [String]
        images: [ImagesInput]
        sizes: [SizesInput]
        prices: [PricesInput]

        brand: String!
        category: String!
        subCategory: String!

        thumbnail: ImageColorInput
        mainImage: ImageColorInput
        relatedProducts: [RelatedProductInput]
    }

    input WhereInput {
        param: [String]
        category: [String]
        subCategory: [String]
        brands: [String]
        colors: [String]
        sizes: [String]
        sortBy: String,
        sortDirection: String
    }

    extend type Query {
        products(category: [String], subCategory: [String], brands: [String], colors: [String], sizes: [String], skip: Int, first: Int): [Product]
        search(term: String!): [Product]
        product(key: ID!): Product
        productByIds(id: String): Product
        productsByParams(where: WhereInput, skip: Int, first: Int): [Product]
    }

    extend type Mutation {
        createProduct(input: ProductInput): Product
        updateProduct(key: ID!, input: ProductInput): Product
        deleteProduct(key: ID!): Product
    }
`;
