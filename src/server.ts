require('dotenv').config();

import express from "express";
import { ApolloServer, AuthenticationError } from "apollo-server-express";
import { createServer } from "http";
import jwt from 'jsonwebtoken';
import Cookies from "cookies";
import compression from "compression";
import cors from "cors";
// import helmet from "helmet";
import { schema } from "./schema"
// import models from './models'
import Data from './models/Data';
require('./config');
console.log('-', process.env.CLOUD_NAME);
console.log('-', process.env.API_KEY);

const PORT = process.env.PORT || 3000;
const app = express();
app.use("*", cors());
// const corsOptions = {
//     origin: 'http://localhost:3001',
//     credentials: true
// }
// app.use(cors(corsOptions))
// app.use(cors({ origin: true, credentials: true }));

const SECRET = 'secret1';
const getMe = (req: any) => {
  const token = req.headers['x-token'];
  //console.log('token in ', token);
  if (!token) return null;
  try {
    return jwt.verify(token, SECRET);
  } catch (e) {
    return null;
    // throw new AuthenticationError(
    //   'Your session expired. Sign in again.',
    // );
  }
};

// app.use(helmet());
app.use(compression());
const server = new ApolloServer({
  introspection: true,
  playground: true,  
  schema,
  context: ({ req, res }) => {
    //const cookies = new Cookies(req, res)
    //const = cookies.get();
    // console.log('token in header', req.header.get['x-token'])
    // // const tokenInCookies: string | undefined = cookies.get('x-token') || undefined;
    // const tokenInHeaders: string | undefined = req.header['x-token'] || undefined;
    //const token = cookies.get("x-token") ? cookies.get('x-token') : req.headers['x-token'];
    // const token  = tokenInCookies || tokenInHeaders

    const me = getMe(req);
    return {
      // models,
      //cookies,
      data: Data.getInstance(),
      me,
      secret: SECRET
    }
  },
});

server.applyMiddleware({ app, path: "/graphql" });
const httpServer = createServer(app);
httpServer.listen({ port: PORT }, (): void =>
  console.log(`🚀GraphQL-Server is running on http://localhost:3000/graphql`)
);