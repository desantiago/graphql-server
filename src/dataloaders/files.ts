const DataLoader = require('dataloader');

import {
    File,
} from '../models/ProductSchema';

const getFiles = async () => {
    const files = await File.find({}).exec();
    return [files]
}

const getFileById = async (ids: string[]) => {
    return await Promise.all(
        ids.map(id => File.findById(id))
    );
}

const files = new DataLoader(() => getFiles());
const fileById = new DataLoader((id: string[]) => getFileById(id));

export default {
    files,
    fileById
};