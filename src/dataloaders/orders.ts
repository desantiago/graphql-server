const DataLoader = require('dataloader');

import {
    Order,
} from '../models/ProductSchema';

const getOrders = async () => {
    return await [Order.find({})];
}

const getOrderById = async (ids: string[]) => {
    return await Promise.all(ids.map(id => Order.findById(id)));
}

const orders = new DataLoader(() => getOrders());
const orderById = new DataLoader((ids: string[]) => getOrderById(ids));

export default {
    orders,
    orderById
};