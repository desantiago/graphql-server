const DataLoader = require('dataloader');

import {
    CartItem,
} from '../models/ProductSchema';

const getItemsByUser = async (users: string[]) => {
    // return await CartItem.find({ 'user': { $in: user } });
    return await Promise.all(users.map(user => CartItem.find({ user })));
}

const getCartItemById = async (ids: string[]) => {
    return await Promise.all(ids.map(id => CartItem.findById(id)));
}

const itemsByUser = new DataLoader((user: string[]) => getItemsByUser(user));
const cartItemById = new DataLoader((id: string[]) => getCartItemById(id));

export default {
    itemsByUser,
    cartItemById
};