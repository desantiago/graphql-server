const DataLoader = require('dataloader');

import {
    Product,
    User,
    Category,
    SubCategory,
    Brand,
    Color,
    File,
} from '../models/ProductSchema';

const getCategories = async (params: any) => {
    return Promise.all(params.map((_: any) => Category.find({})));
    // const cats = await Category.find({}).exec();
    // return [cats]
}

const getCategoryBySlug = async (slugs: string[]) => {
    return Promise.all(slugs.map(slug => Category.findOne({ slug })));
    // return await Category.find({ 'slug': { $in: slug } });
}

const getCategoryById = async (ids: string[]) => {
    return Promise.all(ids.map(id => Category.findById(id)));
    // return await Category.find({ '_id': { $in: id } });
    // const cat = await Category.findById(id).exec();
    // return [cat]
}

const categories = new DataLoader((params: any) => getCategories(params));
const categoryBySlug = new DataLoader((slug: string[]) => getCategoryBySlug(slug));
const categoryById = new DataLoader((id: string[]) => getCategoryById(id));

export default {
    categories,
    categoryBySlug,
    categoryById
};