const DataLoader = require('dataloader');

import {
    OrderItem,
} from '../models/ProductSchema';

const getOrderItems = async () => {
    return await [OrderItem.find({})];
}

const getOrderItemById = async (ids: string[]) => {
    return await Promise.all(ids.map(id => OrderItem.findById(id)));
}

const getOrderItemsByOrder = async (ids: string[]) => {
    return await Promise.all(ids.map(id => OrderItem.find({ order: id })));
}

const orderItems = new DataLoader(() => getOrderItems());
const orderItemById = new DataLoader((ids: string[]) => getOrderItemById(ids));
const orderItemByOrder = new DataLoader((ids: string[]) => getOrderItemsByOrder(ids));

export default {
    orderItems,
    orderItemById,
    orderItemByOrder
};