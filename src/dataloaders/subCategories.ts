const DataLoader = require('dataloader');

import {
    SubCategory,
} from '../models/ProductSchema';

const getSubCategories = async () => {
    const cats = await SubCategory.find({}).exec();
    return [cats]
}

const getSubCategoryBySlug = async (slugs: string[]) => {
    return Promise.all(slugs.map(slug => SubCategory.findOne({ slug })));

    // return await SubCategory.find({ 'slug': { $in: slug } });
    // const cat = await SubCategory.findOne({ slug: slug[0] }).exec();
    // return [cat]
}

const getSubCategoryById = async (ids: string[]) => {
    return Promise.all(ids.map(id => SubCategory.findById(id)));
    //return await SubCategory.find({ '_id': { $in: id } });
    // const cat = await SubCategory.findById(id).exec();
    // return [cat]
}

const getSubCategoriesByCategory = async (category: string[]) => {
    return Promise.all(
        category.map(cat => SubCategory.find({ categoryKey: cat }))
    );

    // let promises: any = [];
    // category.forEach((c: string) => {
    //     promises.push(SubCategory.find({ categoryKey: c }));
    // })

    // const res = await Promise.all(promises);
    // return res;

    // console.log(category);    
    // const d = await SubCategory.find({ 'categoryKey': { $in: category } });
    // console.log(d);
    // return await SubCategory.find({ 'categoryKey': { $in: category } });
    // const cats = await SubCategory.find({ categoryKey: category }).exec();
    // return [cats];
}

const subCategories = new DataLoader(() => getSubCategories());
const subCategoryBySlug = new DataLoader((slug: string[]) => getSubCategoryBySlug(slug));
const subCategoryById = new DataLoader((id: string[]) => getSubCategoryById(id));
const subCategoriesByCategory = new DataLoader(
    (category: string[]) => getSubCategoriesByCategory(category)
);

export default {
    subCategories,
    subCategoryBySlug,
    subCategoryById,
    subCategoriesByCategory,
};