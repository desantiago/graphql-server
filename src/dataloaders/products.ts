// const DataLoader = require('dataloader');
import DataLoader, { BatchLoadFn } from 'dataloader';
import categoryLoaders from './categories';
import subCategoryLoaders from './subCategories';
import brandLoaders from './brands';

import { QueryParams } from '../models/types';
import {
    Product,
    IProduct,
    ICategory,
} from '../models/ProductSchema';

const getQuery = async (params: QueryParams) => {
    // console.log(params)
    const { category, subCategory, brands, colors, sizes, skip, first } = params;
    // console.log("parameters ", category, subCategory, brands, colors, sizes, skip, first);

    let query: any = { $and: [] };

    if (subCategory && Array.isArray(subCategory) && subCategory.length) {
        // console.log("generatin query for subcategories");
        let querySubCat: any = {
            $or: []
        }

        for (const slug of subCategory) {
            const subCat = await subCategoryLoaders.subCategoryBySlug.load(slug);
            querySubCat['$or'].push({
                subCategory: subCat._id
            });
            // console.log(querySubCat);
        }

        query['$and'].push(querySubCat);
    }
    else if (category && Array.isArray(category) && category.length) {

        let queryCat: any = {
            $or: []
        }

        for (const slug of category) {
            const cat = await categoryLoaders.categoryBySlug.load(slug);
            queryCat['$or'].push({
                category: cat._id
            });
        }

        query['$and'].push(queryCat);
    }


    if (brands && Array.isArray(brands) && brands.length) {
        let queryBrands: any = {
            $or: []
        }

        for (const slug of brands) {
            // console.log("getting brand id ", slug);
            const brand = await brandLoaders.brandBySlug.load(slug);
            // console.log("brand", brand);
            queryBrands['$or'].push({
                brand: brand._id
            });
        }
        // console.log(queryBrands);

        query['$and'].push(queryBrands);
    }
    // console.log(query);

    if (colors && Array.isArray(colors) && colors.length) {
        let queryColors: any = {
            $or: []
        }

        for (const id of colors) {
            queryColors['$or'].push({
                colors: id
            });
        }
        // console.log(queryColors);
        query['$and'].push(queryColors);
    }

    if (sizes && Array.isArray(sizes) && sizes.length) {
        let querySizes: any = {
            $or: []
        }

        for (const size of sizes) {
            querySizes['$or'].push({
                'sizes.sizes': size
            });
        }
        query['$and'].push(querySizes);
    }
    return query;
}

const getProducts = async (params: readonly QueryParams[]) => {

    return await Promise.all(params.map(async (param: any) => {
        const { skip, first, sortBy, sortDirection } = param;
        const query = await getQuery(param);

        // console.log("checking pagination", skip, first);
        if (sortBy && sortDirection) {
            const sortObj = {
                [sortBy]: sortDirection === 'ASC' ? 1 : -1
            }

            return await Product
                .find(query)
                .sort(sortObj)
                .skip(skip)
                .limit(first)
                .exec();
        }

        return await Product
            .find(query)
            .skip(skip)
            .limit(first)
            .exec();
    }));

    // const { skip, first } = params[0];
    // const query = await getQuery(params[0]);
}

const getQueryAggregations = (query: any) => {
    return [
        {
            $match: JSON.parse(JSON.stringify(query)),
        },
        {
            $facet: {
                brands: [
                    {
                        $group: {
                            _id: "$brand",
                            count: { $sum: 1 }
                        }
                    },
                    {
                        $project: {
                            _id: "$_id",
                            count: "$count",
                        }
                    }
                ],
                categories: [
                    {
                        $group: {
                            _id: { d: "$subCategory", c: "$category" },
                            count: { $sum: 1 }
                        }
                    },
                    {
                        $group: {
                            _id: "$_id.c",
                            subCategories: {
                                $push: { subCat: "$_id.d" }
                            },
                            count: { $sum: 1 }
                        }
                    }
                ],
                subCategories: [
                    {
                        $group: {
                            _id: "$subCategory",
                            count: { $sum: 1 }
                        }
                    },
                    {
                        $project: {
                            _id: "$_id",
                            count: "$count",
                        }
                    }
                ],
                colors: [
                    {
                        $group: {
                            _id: "$colors",
                            count: { $sum: 1 }
                        }
                    },
                    {
                        $project: {
                            _id: "$_id",
                            count: "$count",
                        }
                    }
                ],
                sizes: [
                    {
                        $project: {
                            "_id": 1,
                            "sizes.sizes": 1
                        }
                    },
                    {
                        $unwind: {
                            path: "$sizes",
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $unwind: {
                            path: "$sizes.sizes",
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $group: {
                            _id: "$sizes.sizes",
                            count: {
                                $sum: 1
                            }
                        }
                    },
                ]
            }
        },
    ];

}

const getAggregations = async (params: any) => {
    return await Promise.all(params.map(async (param: any) => {
        const query = await getQuery(param);
        const aggs = getQueryAggregations(query);
        return await Product.aggregate(aggs);
    }));
}

const getProductById = async (ids: readonly string[]) => {
    return await Promise.all(ids.map(id => Product.findById(id)));
}

const getProductBySlug = async (slugs: readonly string[]) => {
    return await Promise.all(slugs.map(slug => Product.find({ slug })));
}

const getProductByExternalId = async (id: readonly string[]) => {
    const product = await Product.findById({ id }).exec();
    return [product]
}

const getProductByIds = async (id: readonly string[]) => {
    const query = {
        $or: [{ externalId: id[0] }, { slug: id[0] }]
    };

    const product = await Product.findOne(query).exec();
    return [product]
}

const getProductsLike = async (terms: readonly string[]) => {
    return await Promise.all(terms.map(term => Product.find({
        $or: [
            {
                name: {
                    $regex: term,
                    $options: 'i'
                },
            },
            {
                description: {
                    $regex: term,
                    $options: 'i'
                },
            },
            {
                details: {
                    $regex: term,
                    $options: 'i'
                }
            }
        ]
    })));
}

const getParamsFromQuery = async (whereParams: readonly QueryParams[]) => {
    return await Promise.all(whereParams.map(async (where: QueryParams) => {
        const { param } = where;

        if (!param || param?.length === 0)
            return {
                ...where,
                mode: where && where.subCategory && where.subCategory.length ? 'SUBCATEGORY' : 'CATEGORY'
            }

        const cats: ICategory[] = await categoryLoaders.categories.load([]);
        const intersection = cats.filter(cat => param?.includes(cat.slug));
        const mode = intersection.length
            ? where && where.subCategory && where.subCategory.length ? 'SUBCATEGORY' : 'CATEGORY'
            : 'BRAND';

        return {
            ...where,
            category: mode === 'CATEGORY' ? param : where.category,
            brands: mode === 'BRAND' ? param : where.brands,
            mode,
        }
    }));
}


const products = new DataLoader((params: readonly QueryParams[]) => getProducts(params));
const aggregations = new DataLoader((params: readonly QueryParams[]) => getAggregations(params));
const productBySlug = new DataLoader((slug: readonly string[]) => getProductBySlug(slug));
const productById = new DataLoader((id: readonly string[]) => getProductById(id));
const productByExternalId = new DataLoader((id: readonly string[]) => getProductByExternalId(id));
const productByIds = new DataLoader((id: readonly string[]) => getProductByIds(id));
const productsLike = new DataLoader((terms: readonly string[]) => getProductsLike(terms));
const paramsFromQuery = new DataLoader((where: readonly QueryParams[]) => getParamsFromQuery(where));

export default {
    products,
    aggregations,
    productBySlug,
    productById,
    productByExternalId,
    productByIds,
    productsLike,
    paramsFromQuery
};