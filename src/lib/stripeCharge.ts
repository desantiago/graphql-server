import stripe from './stripe';

async function stripeCharge(stripeId: string, total: number) {
    return await stripe.paymentIntents.create({
        amount: total,
        currency: 'usd',
        payment_method: stripeId,
        confirm: true,
        // receipt_email: 'jenny.rosen@example.com',
    }).catch(err => {
        console.log(err);
        throw new Error(err.message);
    });
}

export default stripeCharge;