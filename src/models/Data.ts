import LocalData from './LocalData'
import MongoDBData from './MongoDBData';
import {
    Category,
    SubCategory,
    Brand,
    Product,
    Aggregations2,
    Color,
    File,
    User,
    Token,
    CartItem,
    Order,
    OrderItem,
    OrderItemInput,
    QueryParams
} from './types';

import {
    IBrand,
    ICategory,
    IColor,
    IFile,
    IProduct,
    ISubCategory,
    IUser
} from './ProductSchema';

export default class Data {
    private static instance: Data;
    // private d: LocalData;
    private e: MongoDBData;

    private constructor() {
        // this.d = LocalData.getInstance();
        this.e = MongoDBData.getInstance();
    }

    public static getInstance(): Data {
        if (!Data.instance) {
            Data.instance = new Data();
        }

        return Data.instance;
    }

    public async getCategories(): Promise<ICategory[]> {
        return this.e.getCategories();
    }

    public async getCategory(key: string): Promise<ICategory | null> {
        return await this.e.getCategory(key);
    }

    public async getCategoryBySlug(slug: string): Promise<ICategory | null> {
        return this.e.getCategoryBySlug(slug);
    }

    // public addCategory(description: string, slug: string): Category {
    //     return this.d.addCategory(description, slug);
    // }

    public async getSubCategories(): Promise<ISubCategory[]> {
        return this.e.getSubCategories();
    }

    public async getSubCategory(key: string): Promise<ISubCategory | null> {
        return this.e.getSubCategory(key)
    }

    public async getSubCategoryBySlug(slug: string): Promise<ISubCategory | null> {
        return this.e.getSubCategoryBySlug(slug);
    }

    // public addSubCategory(description: string, slug: string, categoryKey: string): SubCategory {
    //     return this.d.addSubCategory(description, slug, categoryKey);
    // }

    public async getSubCategoriesCategory(categoryKey: string): Promise<ISubCategory[]> {
        return this.e.getSubCategoriesCategory(categoryKey);
    }

    public async getBrands(): Promise<IBrand[]> {
        return this.e.getBrands();
    }

    public async getBrand(key: string): Promise<IBrand | null> {
        return this.e.getBrand(key);
    }

    public async getBrandBySlug(slug: string): Promise<IBrand | null> {
        return this.e.getBrandBySlug(slug);
    }

    public async addBrand(description: string, slug: string): Promise<IBrand | null> {
        return this.e.addBrand(description, slug);
    }

    public async getColors(): Promise<IColor[]> {
        return this.e.getColors();
    }

    public async getColor(key: string): Promise<IColor | null> {
        return this.e.getColor(key);
    }

    public async addColor(name: string, hex: string): Promise<IColor | null> {
        return this.e.addColor(name, hex);
    }

    public async getFiles(): Promise<IFile[]> {
        return this.e.getFiles();
    }

    public async getFile(key: string): Promise<IFile | null> {
        return this.e.getFile(key);
    }

    public async addFile(filename: string, mimetype: string, encondig: string): Promise<IFile | null> {
        return this.e.addFile(filename, mimetype, encondig);
    }

    public async addFileFromCloudinary(input: File): Promise<IFile | null> {
        return this.e.addFileFromCloudinary(input);
    }

    public async deleteFile(key: string): Promise<IFile | null> {
        return this.e.deleteFile(key);
    }

    public async getProducts(category: string[], subcategory: string[], brands: string[], colors: string[], sizes: string[], skip: number, first: number): Promise<IProduct[]> {
        return this.e.getProducts(category, subcategory, brands, colors, sizes, skip, first);
    }

    public async getProductsByParams(where: QueryParams, skip: number, first: number): Promise<IProduct[]> {
        return this.e.getProductsByParams(where, skip, first);
    }

    public async aggregations(category: string[], subcategory: string[], brands: string[], colors: string[], sizes: string[]): Promise<Aggregations2> {
        return this.e.aggregations(category, subcategory, brands, colors, sizes);
    }

    public async aggregationsByParams(where: QueryParams): Promise<Aggregations2> {
        return this.e.aggregationsByParams(where);
    }

    public async aggregationsNoFilters(where: QueryParams): Promise<Aggregations2> {
        return this.e.aggregationsNoFilters(where);
    }

    public async getProduct(key: string): Promise<IProduct | null> {
        return this.e.getProduct(key);
    }

    public async getProductByIds(id: string): Promise<IProduct | null> {
        return this.e.getProductByIds(id);
    }

    public async addProduct(productInput: Product): Promise<IProduct | null> {
        return this.e.addProduct(productInput);
    }

    public async updateProduct(key: string, productInput: Product): Promise<IProduct | null> {
        return this.e.updateProduct(key, productInput);
    }

    public async deleteProduct(key: string): Promise<IProduct | null> {
        return this.e.deleteProduct(key);
    }

    public getUser(key: string): Promise<IUser | null> {
        return this.e.getUser(key);
    }

    public getTemporalUser(key: string): Promise<IUser | null> {
        return this.e.getTemporalUser(key);
    }

    public signUp(name: string, email: string, password: string, secret: string, tempUser: string): Promise<Token> {
        return this.e.signUp(name, email, password, secret, tempUser);
    }

    public signIn(email: string, password: string, secret: string, tempUser: string): Promise<Token | null> {
        return this.e.signIn(email, password, secret, tempUser);
    }

    public signUpTemp(secret: string): Promise<Token> {
        return this.e.signUpTemp(secret);
    }


    public getCartItem(key: string): Promise<CartItem | null> {
        return this.e.getCartItem(key);
    }

    public getItemsUser(userKey: string): Promise<CartItem[]> {
        return this.e.getItemsUser(userKey);
    }

    public addCartItem(quantity: number, color: string, size: string, productKey: string, userKey: string): Promise<CartItem | null> {
        return this.e.addCartItem(quantity, color, size, productKey, userKey);
    }

    public removeCartItem(key: string, user: string): Promise<CartItem | null> {
        return this.e.removeCartItem(key, user);
    }

    public searchProducts(key: string): Promise<IProduct[]> {
        return this.e.searchProducts(key)
    }

    public getOrders(): Promise<Order[]> {
        return this.e.getOrders();
    }

    public getOrder(key: string): Promise<Order | null> {
        return this.e.getOrder(key);
    }

    public addOrder(total: number, user: string, stripeId: string): Promise<Order | null> {
        return this.e.addOrder(total, user, stripeId);
    }

    public getOrderItem(key: string): Promise<OrderItem | null> {
        return this.e.getOrderItem(key);
    }

    public getOrderItemByOrder(orderKey: string): Promise<OrderItem[]> {
        return this.e.getOrderItemByOrder(orderKey);
    }

    public addOrderItem(input: OrderItemInput): Promise<OrderItem | null> {
        return this.e.addOrderItem(input);
    }

    public checkOut(stripeId: string, user: IUser): Promise<Order | null> {
        return this.e.checkOut(stripeId, user);
    }
}