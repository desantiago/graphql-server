export interface User {
    key: string;
    name: string;
    email: string;
    password: string;
    timeStamp?: string;
}

export interface Token {
    token: string;
}

export interface File {
    key: string;
    filename: string;
    mimetype?: string;
    encoding?: string;
    timeStamp?: string;

    asset_id?: string;
    public_id?: string;
    version?: number;
    version_id?: string;
    signature?: string;
    width?: number;
    height?: number;
    format?: string;
    resource_type?: string;
    created_at?: string;
    bytes?: number;
    etag?: string;
    url?: string;
    secure_url?: string;

    server?: string;
}

export interface Category {
    key: string;
    description: string;
    slug: string;
    timeStamp?: string;
}

export interface SubCategory {
    key: string;
    description: string;
    slug: string;
    timeStamp?: string;
    categoryKey: string;
}

export interface Brand {
    key: string;
    description: string;
    slug: string
    timeStamp?: string;
}

export interface Color {
    key: string;
    name: string;
    hex: string;
}

export interface ImageColor {
    color: string;
    image: string;
}

export interface Image {
    file: string;
    order: number;
}

export interface Images {
    color: string;
    images: Image[];
}

export interface Sizes {
    color: string;
    sizes: string[];
}

export interface Price {
    size: string;
    price: number;
}

export interface Prices {
    color: string;
    prices: Price[]
}

export interface Product {
    key: string;

    externalId?: string | undefined;
    slug: string | undefined;

    name: string;
    description: string | undefined;
    sizeAndFit: string | undefined;

    price: number | undefined;
    startSize: number | undefined;
    endSize: number | undefined;

    details: string | undefined;
    colors: string[] | undefined;

    images: Images[] | undefined;
    sizes?: Sizes[];
    prices?: Prices[];

    // images: DynamicObject<string[]>;
    // sizes?: DynamicObject<number[]>;
    // price?: DynamicObject<DynamicObject<number>>;
    timeStamp?: string;
    brand?: string;
    category?: string;
    subCategory?: string;

    thumbnail: ImageColor | undefined;
    mainImage: ImageColor | undefined;
}

export interface AggregationCategory {
    category: Category | null
    subCategories: SubCategory[]
}

export interface Aggregations {
    count: number
    brands: Brand[]
    sizes: string[]
    colors: Color[]
    categories: AggregationCategory[]
}

export interface AggregationCategory2 {
    category: string
    subCategories: string[]
}

export interface Aggregations2 {
    count: number
    brands: string[]
    sizes: string[]
    colors: string[]
    categories: AggregationCategory2[]
    mode?: string
}

export interface CartItem {
    key: string;
    quantity: number;
    color: string;
    size: string;
    price: number;
    total: number;
    user: string;
    product: string;
}

export interface Order {
    key: string;
    numAtCard: number;
    user: string;
    total: number;
    stripeId: string;
    date: string;
    timeStamp: string;
}

export interface OrderItemInput {
    quantity: number;
    color: string;
    size: string;
    price: number;
    total: number;
    user: string;
    product: string;
    order: string;
    cartItem: string;
}

export interface OrderItem {
    key: string;
    quantity: number;
    color: string;
    size: string;
    price: number;
    total: number;
    user: string;
    product: string;
    productName: string;
    order: string;
    cartItem: string;
    timeStamp: string;
}

export interface QueryParams {
    param?: string[];
    mode?: string;
    category?: string[];
    subCategory?: string[];
    brands?: string[];
    colors?: string[];
    sizes?: string[];
    sortBy?: string;
    sortDirection?: string;
    skip?: number;
    first?: number;
}

export interface QueryParamsInput {
    where?: QueryParams,
    skip?: number;
    first?: number;
}

export interface ProductIds {
    term?: string;
    key?: string;
    id?: string;
}