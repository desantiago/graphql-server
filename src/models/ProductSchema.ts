import mongoose, { Schema, Document } from 'mongoose';
import Data from './Data';
import {
    Product,
    User,
    Category,
    SubCategory,
    Brand,
    Color,
    File,
    OrderItem,
    CartItem,
    Order
} from './types';


interface IProduct extends Product, Document { }

const ProductSchema: Schema = new Schema({
    key: String, // String is shorthand for {type: String}
    externalId: String,
    slug: String,
    name: String,
    description: String,
    sizeAndFit: String,
    price: Number,
    startSize: Number,
    endSize: Number,
    details: String,
    colors: { type: [String], index: true },
    sizes: [{ color: String, sizes: [String] }],
    images: [
        {
            color: String,
            images: [{ file: String, order: Number }]
        }
    ],
    prices: [{
        color: String,
        prices: [
            {
                size: String,
                price: Number
            }
        ]
    }],
    timeStamp: String,
    brand: { type: String, index: true },
    category: { type: String, index: true },
    subCategory: { type: String, index: true },
    thumbnail: { color: String, image: String },
    mainImage: { color: String, image: String },
    relatedProducts: [{
        product: String,
        order: Number
    }]
});

const Product = mongoose.model<IProduct>('products', ProductSchema);


interface IUser extends User, Document { }
const UserSchema: Schema = new Schema({
    key: String,
    name: String,
    email: String,
    password: String,
    timeStamp: String,
});
const User = mongoose.model<IUser>('users', UserSchema);

const TemporalUserSchema: Schema = new Schema({
    key: String,
    name: String,
    email: String,
    password: String,
    timeStamp: String,
});
const TemporalUser = mongoose.model<IUser>('tempUsers', TemporalUserSchema);


interface ICategory extends Category, Document { }
const CategorySchema: Schema = new Schema({
    key: String,
    description: String,
    slug: String,
    timeStamp: String
});
const Category = mongoose.model<ICategory>('categories', CategorySchema);


interface ISubCategory extends SubCategory, Document { }
const SubCategorySchema: Schema = new Schema({
    key: String,
    description: String,
    slug: String,
    timeStamp: String,
    categoryKey: String
});
const SubCategory = mongoose.model<ISubCategory>('subcategories', SubCategorySchema);


interface IBrand extends Brand, Document { }
const BrandSchema: Schema = new Schema({
    key: String,
    description: String,
    slug: String,
    timeStamp: String
})
const Brand = mongoose.model<IBrand>('brands', BrandSchema);


interface IColor extends Color, Document { }
const ColorSchema: Schema = new Schema({
    key: String,
    name: String,
    hex: String,
})
const Color = mongoose.model<IColor>('colors', ColorSchema);

interface IFile extends File, Document { }
const FileSchema: Schema = new Schema({
    key: String,
    filename: String,
    mimetype: String,
    encoding: String,
    timeStamp: String,

    asset_id: String,
    public_id: String,
    version: Number,
    version_id: String,
    signature: String,
    width: Number,
    height: Number,
    format: String,
    resource_type: String,
    created_at: String,
    bytes: Number,
    etag: String,
    url: String,
    secure_url: String,
    server: String,
})
const File = mongoose.model<IFile>('files', FileSchema);

interface IOrder extends Order, Document { }
const OrderSchema: Schema = new Schema({
    key: String,
    numAtCard: Number,
    user: String,
    total: Number,
    stripeId: String,
    date: String,
    timeStamp: String
})
const Order = mongoose.model<IOrder>('orders', OrderSchema);

interface IOrderItem extends OrderItem, Document { }
const OrderItemSchema: Schema = new Schema({
    key: String,
    quantity: Number,
    color: String,
    size: String,
    price: Number,
    total: Number,
    user: String,
    product: String,
    productName: String,
    order: String,
    cartItem: String,
    timeStamp: String,
})
const OrderItem = mongoose.model<IOrderItem>('orderItems', OrderItemSchema);

interface ICartItem extends CartItem, Document { }
const CartItemSchema: Schema = new Schema({
    key: String,
    quantity: Number,
    color: String,
    size: String,
    price: Number,
    total: Number,
    user: String,
    product: String,
})
const CartItem = mongoose.model<ICartItem>('cartItems', CartItemSchema);

interface ServerContext {
    data: Data,
    me: IUser,
    secret: string
}

export {
    Product,
    User,
    Category,
    SubCategory,
    Brand,
    Color,
    File,
    Order,
    OrderItem,
    CartItem,
    TemporalUser,

    IProduct,
    IUser,
    ICategory,
    ISubCategory,
    IBrand,
    IColor,
    IFile,
    IOrder,
    IOrderItem,
    ICartItem,

    ServerContext
}
